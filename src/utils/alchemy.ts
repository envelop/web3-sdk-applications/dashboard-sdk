import {
    _AssetType,
    BigNumber,
    combineURLs,
    createAuthToken,
    decodeAssetTypeFromString,
    NFT,
} from "@envelop/envelop-client-core";

export const getUserNFTsFromAlchemy = async (
	chainId: number,
	userAddress: string,
	pageKey?: string,
) => {

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		return [];
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return []; }

	let url = combineURLs(BASE_URL, `/alchemy/remote/${chainId}/getNFTsForOwner?owner=${userAddress}&withMetadata=true&pageSize=100`);
	if ( pageKey ) { url = `${url}&pageKey=${pageKey}`; }

	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		let respParsed = await resp.json();

		if ( !resp.ok ) {
			throw Error(`Cannot fetch token from alchemy, ${respParsed}`);
		}
		if ('error' in respParsed) {
			throw Error(`Cannot fetch token from alchemy, ${respParsed}`);
		}

		return respParsed;

	} catch (e) {
		throw Error(`Cannot fetch token from alchemy, ${e}`);
	}
}