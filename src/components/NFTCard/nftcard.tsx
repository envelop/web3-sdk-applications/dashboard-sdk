
import React, {
	useContext,
	useState
} from "react";
import CopyToClipboard from "react-copy-to-clipboard";

import TippyWrapper    from "../TippyWrapper";
import CoinIconViewer  from "../CoinIconViewer";
import CoinViewer      from "../CoinViewer";
import AddValuePopup   from "../AddValuePopup";
import TransferPopup   from "../TransferPopup";
import ApprovePopup    from "../ApprovePopup";

import {
	BigNumber,
	Lock,
	LockType,
	NFTorWNFT,
	_AssetType,
	addThousandSeparator,
	assetTypeToString,
	castToWNFT,
	compactString,
	tokenToFloat,
	dateToStrWithMonth,
	unixtimeToDate,
} from "@envelop/envelop-client-core"

import {
	ERC20Context,
	Web3Context
} from "../../dispatchers";

import icon_logo           from "../../static/pics/logo-small.svg";
import icon_i_copy         from '../../static/pics/i-copy.svg';
import icon_i_plus         from '../../static/pics/i-plus.svg';
import icon_i_link         from '../../static/pics/i-link.svg';
import icon_i_action       from '../../static/pics/icons/i-dots-hor.svg';

export enum TokenRenderType {
	wrapped,
	discovered,
}
type NFTCardProps = {
	token: NFTorWNFT,
	tokenRenderType: TokenRenderType,
	transferSuccessCallback?: (token: NFTorWNFT) => void,
}

export default function NFTCard(props: NFTCardProps) {

	const {
		token,
		tokenRenderType,
		transferSuccessCallback,
	} = props;

	const tokensListBlockRef = React.useRef<HTMLInputElement>(null);
	const v0AppLink = 'https://appv0.envelop.is';

	const {
		userAddress,
		currentChain,
		currentChainId,
	} = useContext(Web3Context);
	const {
		erc20List
	} = useContext(ERC20Context);

	const userMenuBlockRef = React.useRef<HTMLInputElement>(null);
	const cursorOnCardMenu = React.useRef(false);

	const [ menuOpened,    setMenuOpened    ] = useState(false);
	// const [ cursorOnCardMenu, setCursorOnCardMenu ] = useState(false);

	const [ tokensListOpened, setTokensListOpened ] = useState(false);

	const [ addValuePopup, setAddValuePopup ] = useState(false);
	const [ transferPopup, setTransferPopup ] = useState(false);
	const [ approvePopup,  setApprovePopup  ] = useState(false);

	// ----- COPYABLE -----
	let   copiedHintTimer   = 0;
	const copiedHintTimeout = 2; // s
	const [ copiedHintWhere, setCopiedHintWhere ] = useState('');
	const getCopiedHint = (where: string) => {
		if ( copiedHintWhere === where  ) {
			return (<span className="btn-action-info">Copied</span>)
		}
	}
	// ----- END COPYABLE -----

	// ----- HEADER -----
	const getContractAddressBlock = () => {
		return (
			<CopyToClipboard
				text={ token.contractAddress }
				onCopy={() => {
					setCopiedHintWhere('address');
					clearTimeout(copiedHintTimer);
					copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
				}}
			>
				<button className="btn-copy">
					<TippyWrapper msg={ token.contractAddress }>
						<span>
							<span className="title">ADDRESS</span>
							{ compactString(token.contractAddress) }
						</span>
					</TippyWrapper>
					<img src={ icon_i_copy } alt="" />
					{ getCopiedHint('address') }
				</button>
			</CopyToClipboard>
		)
	}
	const getIdBlock = () => {
		if ( `${token.tokenId}`.length < 8 ) {
			return (
				<CopyToClipboard
					text={ token.tokenId }
					onCopy={() => {
						setCopiedHintWhere('id');
						clearTimeout(copiedHintTimer);
						copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
					}}
				>
					<button className="btn-copy">
						<span>
							<span className="title">ID</span>
							{ compactString(token.tokenId) }
						</span>
						<img src={icon_i_copy} alt="" />
						{ getCopiedHint('id') }
					</button>
				</CopyToClipboard>
			)
		}

		return (
			<CopyToClipboard
				text={ token.tokenId }
				onCopy={() => {
					setCopiedHintWhere('id');
					clearTimeout(copiedHintTimer);
					copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
				}}
			>
				<button className="btn-copy">
					<TippyWrapper msg={ token.tokenId } >
						<span>
							<span className="title">ID</span>
							{ compactString(token.tokenId) }
						</span>
					</TippyWrapper>
					<img src={icon_i_copy} alt="" />
					{ getCopiedHint('id') }
				</button>
			</CopyToClipboard>
		)
	}
	const getCardHeader = () => {
		return (
			<div>
				{ getContractAddressBlock() }
				{ getIdBlock() }
			</div>
		)
	}
	// ----- MENU -----
	const getMenuItemCopy = () => {
		if ( !currentChain ) { return null; }
		return (
			<li>
				<CopyToClipboard
					text={ `${window.location.origin}/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}` }
					onCopy={() => {
						setCopiedHintWhere('tokenlinkmenu');
						clearTimeout(copiedHintTimer);
						copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
					}}
				>
					<button>
						Copy NFT URL
						{ getCopiedHint('tokenlinkmenu') }
					</button>
				</CopyToClipboard>
			</li>
		)
	}
	const getMenuItemTransfer = () => {
		const getTransferFeeLabel = () => {
			if ( !currentChain ) { return null; }
			if ( !token.fees || !token.fees.length ) { return null; }

			if ( token.fees[0].token.toLowerCase() === '0x0000000000000000000000000000000000000000' ) {
				return (
					<span className="data">
						{ addThousandSeparator(tokenToFloat(token.fees[0].value, currentChain.decimals).toString()) }
						{ '  ' }
						{ currentChain.symbol }
					</span>
				)
			}

			const foundToken = erc20List.find((item) => {
				if ( !token || !token.fees ) { return false; }
				return item.contractAddress.toLowerCase() === token.fees[0].token.toLowerCase()
			});
			if ( foundToken ) {
				return (
					<span className="data">
						{ addThousandSeparator(tokenToFloat(token.fees[0].value, foundToken.decimals || 18).toString()) }
						{ '  ' }
						{ foundToken.symbol }
					</span>
				)
			}

		}

		if ( !userAddress ) { return null; }

		if (
			( token.assetType === _AssetType.ERC721  && !token.owner ) ||
			( token.assetType === _AssetType.ERC1155 && !token.owner )
		) {
			return (
				<TippyWrapper
					msg="Cannot check ownership"
				>
					<li>
						<button
							disabled={ true }
						>
							Transfer
						</button>
					</li>
				</TippyWrapper>
			)
		}

		if ( token.assetType === _AssetType.ERC721  && token.owner && token.owner.toLowerCase() !== userAddress.toLowerCase() ) { return null; }
		if ( token.assetType === _AssetType.ERC1155 && token.amount && token.amount.lt(0) ) { return null; }

		return (
			<li>
				<button
					onClick={() => {
						setTransferPopup(true);
					}}
				>
					Transfer
					<span className="data">
						{ getTransferFeeLabel() }
					</span>
				</button>
			</li>
		)

	}
	const getMenuItemApprove = () => {
		return (
			<li>
				<button
					onClick={() => { setApprovePopup(true) }}
				>
					Set approval for all tokens of contract
				</button>
			</li>
		)
	}
	const getMenuItemOpen = () => {
		return (
			<li>
				<button
					onClick={() => {
						window.location.href = `/token/${currentChainId}/${token.contractAddress}/${token.tokenId}`;
					}}
				>
					View token
				</button>
			</li>
		)
	}
	const openCardMenu = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				if ( !userMenuBlockRef.current ) { return; }
				const _path = e.composedPath() || e.path;
				if ( _path && _path.includes(userMenuBlockRef.current) ) { return; }
				closeCardMenu();
			};
		}, 100);
		setMenuOpened(true);
	}
	const closeCardMenu = () => {
		setTimeout(() => {
			if ( cursorOnCardMenu.current ) { return; }
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = null;
			setMenuOpened(false);
		}, 100);
	}
	const getCardMenuList = () => {
		if ( menuOpened ) {
			return (
				<ul
					className="list-action"
					onMouseEnter={() => {
						openCardMenu();
						cursorOnCardMenu.current = true;
					}}
					onMouseLeave={() => {
						cursorOnCardMenu.current = false;
						closeCardMenu();
					}}
				>
					{ getMenuItemCopy() }
					{ getMenuItemTransfer() }
					{ getMenuItemApprove() }
					{ getMenuItemOpen() }
				</ul>
			)
		}
	}
	const getCardMenu = () => {
		return (
			<div
				className="w-card__action"
				ref={ userMenuBlockRef }
			>
				<button
					className="btn-action"
					onMouseEnter={ openCardMenu }
					onMouseLeave={ closeCardMenu }
					onClick={ openCardMenu }
				>
					<img src={icon_i_action} alt="" />
				</button>

				{ getCardMenuList() }
			</div>
		)
	}
	// ----- END MENU -----
	// ----- END HEADER -----

	const getMedia = () => {
		if ( !currentChain ) { return null; }

		if (
			!token.tokenUrl ||
			token.tokenUrl === '' ||
			!token.image ||
			token.image === ''
		) {
			return (
				<a
					className="inner"
					href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
				>
					<div className="default">
						<img src={ icon_logo } alt="" />
						<span>Cannot load NON-FUNGIBLE <br /> TOKEN Preview</span>
					</div>
				</a>
			)
		}

		if ( token.tokenUrl?.startsWith('service:')  ) {

			const urlParams = token.tokenUrl.split(':');
			if ( urlParams[1].toLowerCase() === 'blocked' ) {
				return (
					<a
						className="inner"
						href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
					>
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Token url loading blocked</span>
						</div>
					</a>
				)
			}

			if ( urlParams[1].toLowerCase() === 'loading' ) {
				return (
					<a
						className="inner"
						href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
					>
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Loading NON-FUNGIBLE <br /> TOKEN Preview</span>
						</div>
					</a>
				)
			}

			if ( urlParams[1].toLowerCase() === 'cannotload' ) {
				return (
					<a
						className="inner"
						href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
					>
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Cannot load NON-FUNGIBLE <br /> TOKEN Preview</span>
						</div>
					</a>
				)
			}

			if ( urlParams[1].toLowerCase() === 'nourl' ) {
				return (
					<a
						className="inner"
						href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
					>
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Click on the NFT card to see the metadata</span>
						</div>
					</a>
				)
			}
		}

		if ( token.image?.startsWith('service:')  ) {

			const urlParams = token.image.split(':');
			if ( urlParams[1].toLowerCase() === 'blocked' ) {
				return (
					<a
						className="inner"
						href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
					>
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Image url loading blocked</span>
						</div>
					</a>
				)
			}

			if ( urlParams[1].toLowerCase() === 'loading' ) {
				return (
					<a
						className="inner"
						href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
					>
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Loading NON-FUNGIBLE <br /> TOKEN Preview</span>
						</div>
					</a>
				)
			}

			if ( urlParams[1].toLowerCase() === 'cannotload' ) {
				return (
					<a
						className="inner"
						href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
					>
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Cannot load NON-FUNGIBLE <br /> TOKEN Preview</span>
						</div>
					</a>
				)
			}
			if ( urlParams[1].toLowerCase() === 'nourl' ) {
				return (
					<a
						className="inner"
						href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
					>
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Click on the NFT card to see the metadata</span>
						</div>
					</a>
				)
			}
		}

		return (
			<a
				className="inner"
				href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
			>
				<video className="img" src={ token.image } poster={ token.image } autoPlay={ true } muted={ true } loop={ true } />
			</a>
		)
	}

	const getType = () => {
		if ( !currentChain ) { return; }
		if ( token.assetType ) {
			return ( <span className="w-card__tag" style={{ textTransform: 'none' }}>{ assetTypeToString(token.assetType, currentChain.EIPPrefix) }</span> )
		}

		return null;
	}
	const getSaleStatus = () => {
		if ( !currentChain ) { return; }
		if ( token.sale_status && token.sale_status === 1 ) {
			return ( <span className="w-card__tag">On sale</span> )
		}

		return null;
	}
	const getCopyButton = () => {
		if ( !currentChain ) { return; }

		return (
			<CopyToClipboard
				text={ `${window.location.origin}/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}` }
				onCopy={() => {
					setCopiedHintWhere('tokenlinkimg');
					clearTimeout(copiedHintTimer);
					copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
				}}
			>
				<button className="btn-token-link">
					<img src={ icon_i_link } alt="" />
					{ getCopiedHint('tokenlinkimg') }
				</button>
			</CopyToClipboard>
		)
	}
	const getExtra = () => {
		return (
			<div className="extra">
				{ getType() }
				{ getSaleStatus() }
				{ getCopyButton() }
			</div>
		)
	}

	const closeTokenList = () => {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = null;
		setTokensListOpened(false);
	}
	const openTokenList = () => {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = (e: any) => {
			if ( !tokensListBlockRef.current ) { return; }
			const _path = e.composedPath() || e.path;
			if ( _path && _path.includes(tokensListBlockRef.current) ) { return; }
			closeTokenList();
		};
		setTokensListOpened(true);
	}
	const getCollateralBlock = () => {

		if ( tokenRenderType !== TokenRenderType.wrapped ) { return null; }
		if ( !token.collateral ) { return null; }

		const ITEMS_TO_SHOW = 5;
		let additionalAssetssQty = token.collateral.length - ITEMS_TO_SHOW;

		return (
			<div className="w-card__param">
			<div className="form-row">
			<div className="field-wrap">
				<div className="field-row">
					<label className="field-label">Collateral</label>
				</div>
				<div className="field-row">
					<div
						className="field-control field-collateral"
						ref={ tokensListBlockRef }
						onClick={ openTokenList }
						onMouseEnter={ openTokenList }
						onMouseLeave={ closeTokenList }
					>
						<CoinIconViewer
							tokens = { token.collateral.slice(0,ITEMS_TO_SHOW) }
						/>
						{
							token.collateral.length === 0 ?
							(
								<div className="sum">no assets</div>
							) : null
						}
						{
							token.collateral.length > ITEMS_TO_SHOW ?
							(
								<span className="more-assets">
									{ '+' }
									{ additionalAssetssQty || '' }
									{ ' ' }
									{ additionalAssetssQty > 0 ? ( additionalAssetssQty === 1 ? 'asset' : 'assets' ) : 'no assets' }
								</span>
							) : null
						}
						{
							tokensListOpened && token.collateral.length ?
								<CoinViewer
									tokens = { token.collateral }
								/>
								: null
						}
					</div>
					{
						tokenRenderType === TokenRenderType.wrapped && token.rules && !token.rules.noAddCollateral ?
						(
							<button
								className="btn-add"
								onClick={() => { setAddValuePopup(true); }}
							>
								<img src={ icon_i_plus } alt="" />
							</button>
						) : ''
					}
				</div>
			</div>
			</div>
			</div>
		)
	}

	const getCardFooter = () => {
		if ( !currentChain ) { return; }
		if ( token.assetType === _AssetType.wNFTv0 ) {
			return (
				<a
					className="btn btn-lg btn-yellow"
					target="_blank" rel="noopener noreferrer"
					href={`${v0AppLink}/#/token?chain=${currentChain.chainId}&contractAddress=${token.contractAddress}&tokenId=${token.tokenId}`}
				>Unwrap in v0 app</a>
			)
		}

		if ( tokenRenderType === TokenRenderType.discovered ) {
			return (
				<a
					className="btn btn-lg btn-primary"
					href={`/wrap/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
				>WRAP</a>
			)
		}

		const conditions: Array<string> = [];

		if ( token.rules?.noUnwrap ) {
			conditions.push('No unwrap')
		}

		if ( token.locks ) {
			token.locks.forEach((item: Lock) => {

				if ( item.lockType === LockType.time ) {
					const nowDate = new BigNumber(new Date().getTime()).dividedToIntegerBy(1000);
					if ( item.param.gt( nowDate ) ) { conditions.push(dateToStrWithMonth(unixtimeToDate(item.param))) }
				}

				if ( item.lockType === LockType.value ) {

					if ( !token.fees ) { return null; }

					if ( token.fees.length && token.collectedFees && item.param.gt(token.collectedFees) ) {
						const foundToken = erc20List.find((iitem) => {
							if ( !token.fees ) { return false; }
							return iitem.contractAddress.toLowerCase() === token.fees[0].token.toLowerCase()
						});
						if ( !foundToken ) { return null; }

						const diff = tokenToFloat(item.param.plus(-token.collectedFees), foundToken.decimals || 18);
						conditions.push(`Collect fee ${addThousandSeparator(diff.toString())} ${foundToken.symbol} to unwrap`)
					}
				}

			})
		}

		if (conditions.length) {
			return (
				<div className="w-card__status">
					<div>
						<span className="small">{ 'Cannot unwrap token:' }</span>
						<span>{ conditions.join(', ') }</span>
					</div>
				</div>
			)
		}

		return (
			<a
				className="btn btn-lg btn-yellow"
				href={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
			>UNWRAP</a>
		)
	}


	return (
		<>
		<div className="w-card w-card-preview" key={`${token.contractAddress}${token.tokenId}`}>
			<div className="bg">
				<div className="w-card__meta">
					{ getCardHeader() }
					{ getCardMenu() }
				</div>

				<div className="w-card__token">
					{ getExtra() }
					{ getMedia() }
				</div>

				{ getCollateralBlock() }

				{ getCardFooter() }
			</div>
		</div>
		{
			addValuePopup ?
			(
				<AddValuePopup
					token={ castToWNFT(token) }
					closePopup={()=>{ closeCardMenu(); setAddValuePopup(false); }}
				/>
			)
			: null
		}
		{
			transferPopup ?
			(
				<TransferPopup
					token={ token }
					closePopup={()=>{ closeCardMenu(); setTransferPopup(false); }}
					successCallback={transferSuccessCallback}
				/>
			)
			: null
		}
		{
			approvePopup ?
			(
				<ApprovePopup
					token={ token }
					closePopup={()=>{ closeCardMenu(); setApprovePopup(false); }}
				/>
			)
			: null
		}
		</>
	)
}