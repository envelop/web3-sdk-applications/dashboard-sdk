
import React, {
	useContext,
	useState
} from 'react';
import {
	BigNumber,
	ChainType,
	NFT,
	NFTorWNFT,
	Web3,
	_AssetType,
	combineURLs,
	getChainId,
	getERC20BalanceFromChain,
	getNullERC20,
	getWNFTById,
	getWNFTWrapperContract,
	getWrapperTechToken,
	isContractWNFTFromChain,
	localStorageGet,
	makeERC20Allowance,
	makeERC20AllowanceMultisig,
	tokenToFloat,
	transferERC1155,
	transferERC721
} from '@envelop/envelop-client-core';
import {
	ERC20Context,
	InfoModalContext,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes
} from '../../dispatchers';

type TransferPopupProps = {
	token           : NFT,
	closePopup      : Function,
	successCallback?: (token: NFTorWNFT) => void,
}

export default function TransferPopup(props: TransferPopupProps) {

	const {
		token,
		closePopup,
		successCallback,
	} = props;

	const v0AppLink = 'https://appv0.envelop.is';

	const [ inputTransferAddress, setInputTransferAddress ] = useState('');
	const [ inputTransferAmount,  setInputTransferAmount  ] = useState('');

	const {
		currentChain,
		web3,
		userAddress,
		getWeb3Force,
	} = useContext(Web3Context);
	const {
		setLoading,
		unsetModal,
		setModal,
		createAdvancedLoader,
		updateStepAdvancedLoader,
	} = useContext(InfoModalContext);
	const {
		erc20List,
	} = useContext(ERC20Context);

	const isTransferBtnDisabled = () => {
		if ( inputTransferAddress === '' ) { return true; }
		if ( !Web3.utils.isAddress(inputTransferAddress) ) { return true; }

		if ( token ) {
			if ( token.assetType === _AssetType.ERC1155 ) {
				if ( inputTransferAmount === '' ) { return true; }
				if ( token.amount && token.amount.lt(new BigNumber(inputTransferAmount)) ) { return true; }
			}
		}
	}
	const transferSubmit = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		createAdvancedLoader({
			title: 'Waiting to transfer',
			stages: [
				{
					id: 'transfer',
					sortOrder: 20,
					text: 'Transferring token',
					status: _AdvancedLoadingStatus.queued
				}
			]
		});

		const isWNFT = await isContractWNFTFromChain(_currentChain.chainId, token.contractAddress, token.tokenId);
		if ( isWNFT ) {
			createAdvancedLoader({
				title: 'Waiting to transfer',
				stages: [
					{
						id: 'approvefee',
						sortOrder: 10,
						text: 'Approving transfer fee',
						status: _AdvancedLoadingStatus.loading
					},
					{
						id: 'transfer',
						sortOrder: 20,
						text: 'Transferring token',
						status: _AdvancedLoadingStatus.queued
					}
				]
			});
			const wnft = await getWNFTById(_currentChain.chainId, token.contractAddress, token.tokenId);
			if ( wnft && wnft.fees && wnft.fees.length ) {
				const fee = wnft.fees[0];
				let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === fee.token.toLowerCase() });
				if ( !foundToken ) { foundToken = getNullERC20(fee.token) }

				const wrapperContract = await getWNFTWrapperContract(_currentChain.chainId, token.contractAddress);
				const techToken = await getWrapperTechToken(_currentChain.chainId,  wrapperContract);

				if ( techToken.toLowerCase() !== fee.token.toLowerCase() ) {

					updateStepAdvancedLoader({
						id: 'approvefee',
						text: `Approving (${foundToken.symbol})`,
						status: _AdvancedLoadingStatus.loading
					});

					const balance = await getERC20BalanceFromChain(_currentChain.chainId, fee.token, _userAddress, wrapperContract);
					if ( balance.balance.lt(fee.value) ) {
						setModal({
							type: _ModalTypes.error,
							title: `Not enough ${foundToken.symbol}`,
							details: [
								`Token ${foundToken.symbol}: ${fee.token}`,
								`User address: ${_userAddress}`,
								`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
							]
						});
						return;
					}
					if ( !balance.allowance || balance.allowance.amount.lt(fee.value) ) {
						try {
							if ( isMultisig ) {
								await makeERC20AllowanceMultisig(_web3, fee.token, _userAddress, fee.value, wrapperContract);
							} else {
								await makeERC20Allowance(_web3, fee.token, _userAddress, fee.value, wrapperContract);
							}
						} catch(e: any) {
							setModal({
								type: _ModalTypes.error,
								title: `Cannot approve ${foundToken.symbol}`,
								details: [
									`Token ${foundToken.symbol}: ${fee.token}`,
									`User address: ${_userAddress}`,
									`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
									'',
									e.message || e,
								]
							});
							return;
						}
					}
				}
			}
			updateStepAdvancedLoader({
				id: 'approvefee',
				status: _AdvancedLoadingStatus.complete,
			});
		}

		updateStepAdvancedLoader({
			id: 'transfer',
			status: _AdvancedLoadingStatus.loading,
		});

		let txResp: any;
		try {
			if ( token.assetType === _AssetType.ERC1155 ) {
				txResp = await transferERC1155(
					_web3,
					token.contractAddress,
					token.tokenId,
					new BigNumber(inputTransferAmount),
					_userAddress,
					inputTransferAddress
				);
			} else {
				txResp = await transferERC721(
					_web3,
					token.contractAddress,
					token.tokenId,
					_userAddress,
					inputTransferAddress
				);
			}
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot transfer token`,
				details: [
					`Token ${token.contractAddress}: ${token.tokenId}`,
					`User address: ${_userAddress}`,
					'',
					e.message || e,
				]
			});
			return;
		}
		updateStepAdvancedLoader({
			id: 'transfer',
			status: _AdvancedLoadingStatus.complete
		});
		unsetModal();

		if ( isMultisig ) {
			setModal({
				type: _ModalTypes.success,
				title: `Transactions have been created`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						if ( successCallback ) { successCallback(token); }
					}
				},{
					text: 'To token page',
					clickFunc: () => {
						window.location.href = `/token/${token.chainId}/${token.contractAddress}/${token.tokenId}`;
					},
					size: 'xl'
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		} else {
			setModal({
				type: _ModalTypes.success,
				title: `Token successfully transferred`,
				text: currentChain?.hasOracle ? [
					{ text: 'After oracle synchronization, your changes will appear on dashboard soon. Please refresh the dashboard page' },
				] : [],
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						if ( successCallback ) { successCallback(token); }
					}
				},{
					text: 'To token page',
					clickFunc: () => {
						window.location.href = `/token/${token.chainId}/${token.contractAddress}/${token.tokenId}`;
					},
					size: 'xl'
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		}
		closePopup();
	}
	const getTransferSubmitBtn = () => {
		return (
			<button
				className="btn"
				type="button"
				disabled={ isTransferBtnDisabled() }
				onClick={async (e) => {
					if ( !currentChain ) { return; }
						let _web3 = web3;
						let _userAddress = userAddress;
						let _currentChain = currentChain;

						if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
							setLoading('Waiting for wallet');

							try {
								const web3Params = await getWeb3Force(_currentChain.chainId);

								_web3 = web3Params.web3;
								_userAddress = web3Params.userAddress;
								_currentChain = web3Params.chain;
								unsetModal();
							} catch(e: any) {
								setModal({
									type: _ModalTypes.error,
									title: 'Error with wallet',
									details: [
										e.message || e
									]
								});
							}
						}

						if ( !_web3 || !_userAddress || !_currentChain ) { return; }
						const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
						transferSubmit(_currentChain, _web3, _userAddress, isMultisig);
				}}
			>Accept</button>
		)

	}
	const getV0Label = () => {
		if ( !currentChain ) { return null; }
		if ( token.assetType !== _AssetType.wNFTv0 ) { return null; }

		const tokenUrl = `${v0AppLink}/#/token?chain=${currentChain.chainId || 0}&contractAddress=${ token.contractAddress }&tokenId=${ token.tokenId }`

		return (
			<React.Fragment>
				<p className="text-orange">This NFT has been wrapped by the previous version of dApp. Some functions may not work properly with current version of dApp.<br /><a target="_blank" rel="noopener noreferrer" href={ tokenUrl } >Go to v0 app.</a></p>
			</React.Fragment>
		)
	}

	return (
		<div className="modal">
			<div
				className="modal__inner"
				onClick={(e) => {
					e.stopPropagation();
					if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
						closePopup();
					}
				}}
			>
				<div className="modal__bg"></div>
				<div className="container">
					<div className="modal__content">
						<div
							className="modal__close"
							onClick={() => { closePopup() }}
						>
							<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
								<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
							</svg>
						</div>
						<div className="c-add">
							<div className="c-add__text">
								<div className="h2">Transfer token</div>
								<p>After this action you will not own this wrapped NFT</p>
								{ getV0Label() }
							</div>
							<div className="c-add__coins">
								<div className="c-add__form">

										<div className="form-row">
											<div className="col">
												<input
													className="input-control"
													type="text"
													placeholder={ 'Paste address' }
													value={ inputTransferAddress }
													onChange={(e) => { setInputTransferAddress(e.target.value) }}
												/>
											</div>
											{
												token && token.assetType  === _AssetType.ERC1155  ? (
													<div className="col">
														<input
															className="input-control"
															type="text"
															placeholder={ 'Enter amount' }
															value={ inputTransferAmount }
															onChange={(e) => {
																const value = e.target.value.replaceAll(' ', '');
																if (
																	value === '' ||
																	isNaN(parseInt(value))
																) {
																	setInputTransferAmount('')
																	return;
																}

																setInputTransferAmount(`${parseInt(value)}`)
															}}
														/>
													</div>
												) : null
											}
											<div className="col">
												{ getTransferSubmitBtn() }
											</div>
										</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}