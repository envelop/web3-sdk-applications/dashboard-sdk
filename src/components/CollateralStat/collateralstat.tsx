
import { useContext, useState } from "react";
import {
	BigNumber,
	CollateralItem,
	ERC20Type,
	NFT,
	WNFTsStat,
	_AssetType,
	assetTypeToString,
	chainTypeToERC20,
	compactString,
	getNFTById,
	getNullERC20,
	tokenToFloat,
} from "@envelop/envelop-client-core";
import {
	ERC20Context,
	Web3Context
} from "../../dispatchers";

type CollateralStatProps = {
	collateralStat: WNFTsStat,
}

import default_nft  from '../../static/pics/coins/_default_nft.svg';

export default function CollateralStat(props: CollateralStatProps) {

	const {
		collateralStat,
	} = props;

	const [ nfts, setNfts ] = useState<Array<NFT>>([]);
	const [ pendingNfts, setPendingNfts ] = useState<Array<{ contractAddress: string, tokenId: string }>>([]);

	const {
		currentChain,
	} = useContext(Web3Context);
	const {
		erc20List,
		requestERC20Token,
	} = useContext(ERC20Context);

	const getCollateralStatItem = (item: CollateralItem) => {
		if ( !currentChain ) { return null; }

		if ( item.assetType === _AssetType.native ) {
			return (
				<div className="item" key={ `${item.contractAddress}${item.tokenId}` }>
					<div className="row">
						<div className="col-12 col-sm-3 col-md-2 mb-2">
							<div className="tb-coin">
								<span className="i-coin">
									<img src={ currentChain.tokenIcon } alt="" />
								</span>
								<span className="name">{ currentChain.symbol }</span>
							</div>
						</div>
						<div className="col-12 col-sm mb-2"><span className="text-break">{ tokenToFloat(item.amount || new BigNumber(0), currentChain.decimals).toString() }</span></div>
						{/* <div className="col-12 col-sm-3 mb-2 text-right"><span className="text-muted">~ 100 DAI</span></div> */}
					</div>
				</div>
			)
		}
		if ( item.assetType === _AssetType.ERC20 ) {
			let foundToken: ERC20Type | undefined;
			if ( item.contractAddress === '0x0000000000000000000000000000000000000000' ) {
				foundToken = chainTypeToERC20(currentChain);
			} else {
				foundToken = erc20List.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() });
			}
			if ( !foundToken ) {
				foundToken = getNullERC20(item.contractAddress);
				requestERC20Token(item.contractAddress);
			}

			return (
				<div className="item" key={ `${item.contractAddress}${item.tokenId}` }>
					<div className="row">
						<div className="col-12 col-sm-3 col-md-2 mb-2">
							<div className="tb-coin">
								<span className="i-coin">
									<img src={ foundToken.icon } alt="" />
								</span>
								<span className="name">{ foundToken.symbol }</span>
							</div>
						</div>
						<div className="col-12 col-sm mb-2"><span className="text-break">{ tokenToFloat(item.amount || new BigNumber(0), foundToken.decimals).toString() }</span></div>
						{/* <div className="col-12 col-sm-3 mb-2 text-right"><span className="text-muted">~ 100 DAI</span></div> */}
					</div>
				</div>
			)
		}
		if ( item.assetType === _AssetType.ERC721 ) {

			let image = default_nft;
			let label = `${ assetTypeToString(item.assetType, currentChain.EIPPrefix) } ${ compactString(item.contractAddress) }`

			if ( item.contractAddress === '0x0000000000000000000000000000000000000000' ) { return; }
			if ( !item.tokenId ) { return; }

			const token = nfts.find((iitem) => {
				if ( !item.tokenId ) { return; }
				return iitem.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() &&
					iitem.tokenId.toLowerCase() === item.tokenId.toLowerCase()
			});
			if ( token ) {
				image = token.image || default_nft;
				label = token.name || compactString(item.contractAddress)
			} else {

				const pendingToken = pendingNfts.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() && item.tokenId === iitem.tokenId });
				if ( pendingToken ) {
					image = default_nft;
					label = compactString(item.contractAddress);
				} else {

					setPendingNfts((prevState) => {
						return [
							...prevState,
							{ contractAddress: item.contractAddress, tokenId: item.tokenId || '' }
						]
					});
					getNFTById(currentChain.chainId, item.contractAddress, item.tokenId || '')
						.then((data) => {
							if ( !data ) { return; }

							setPendingNfts((prevState) => {
								return prevState.filter((iitem) => {
									return item.contractAddress.toLowerCase() !== iitem.contractAddress.toLowerCase() ||
									(item.tokenId || '').toLowerCase() !== (iitem.tokenId || '').toLowerCase()
								})
							});

							setNfts((prevState) => {
								return [
									...prevState.filter((iitem) => { return item.contractAddress.toLowerCase() !== iitem.contractAddress.toLowerCase() }),
									data
								]
							})
						})
				}
			}

			if ( token ) {

				if ( token.image?.startsWith('service:')  ) {

					const urlParams = token.image.split(':');
					if ( urlParams[1].toLowerCase() === 'blocked' ) {
						image = default_nft;
					}

					if ( urlParams[1].toLowerCase() === 'loading' ) {
						image = default_nft;
					}

					if ( urlParams[1].toLowerCase() === 'cannotload' ) {
						image = default_nft;
					}
					if ( urlParams[1].toLowerCase() === 'nourl' ) {
						image = default_nft;
					}
				} else {
					image = token.image || default_nft;
				}

				label = token.name || compactString(item.contractAddress);
			}

			return (
				<div className="item" key={ `${item.contractAddress}${item.tokenId}` }>
					<div className="row">
						<div className="col-12 col-sm-3 col-md-2 mb-2">
							<div className="tb-coin">
								<span className="i-coin">
									<img src={ image } alt="" />
								</span>
								<span className="name">{ label }</span>
							</div>
						</div>
						<div className="col-12 col-sm mb-2"><span className="text-break">1</span></div>
						{/* <div className="col-12 col-sm-3 mb-2 text-right"><span className="text-muted">~ 100 DAI</span></div> */}
					</div>
				</div>
			)
		}

		if ( item.assetType === _AssetType.ERC1155 ) {

			let image = default_nft;
			let label = `${ assetTypeToString(item.assetType, currentChain.EIPPrefix) } ${ compactString(item.contractAddress) }`
			let amount = '1';

			if ( item.contractAddress === '0x0000000000000000000000000000000000000000' ) { return; }
			if ( !item.tokenId ) { return; }

			const token = nfts.find((iitem) => {
				if ( !item.tokenId ) { return; }
				return iitem.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() &&
					iitem.tokenId.toLowerCase() === item.tokenId.toLowerCase()
			});
			if ( token ) {
				image = token.image || default_nft;
				label = token.name || compactString(item.contractAddress)
				amount = token.amount?.toString() || '1';
			} else {

				const pendingToken = pendingNfts.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() && item.tokenId === iitem.tokenId });
				if ( pendingToken ) {
					image = default_nft;
					label = compactString(item.contractAddress);
					amount = '1';
				} else {

					setPendingNfts((prevState) => {
						return [
							...prevState,
							{ contractAddress: item.contractAddress, tokenId: item.tokenId || '' }
						]
					});
					getNFTById(currentChain.chainId, item.contractAddress, item.tokenId || '')
						.then((data) => {
							if ( !data ) { return; }

							setPendingNfts((prevState) => {
								return prevState.filter((iitem) => {
									return item.contractAddress.toLowerCase() !== iitem.contractAddress.toLowerCase() ||
									(item.tokenId || '').toLowerCase() !== (iitem.tokenId || '').toLowerCase()
								})
							});

							setNfts((prevState) => {
								return [
									...prevState.filter((iitem) => { return item.contractAddress.toLowerCase() !== iitem.contractAddress.toLowerCase() }),
									data
								]
							})
						})
				}
			}

			if ( token ) {
				image = token.image || default_nft;
				label = token.name || compactString(item.contractAddress);
			}

			return (
				<div className="item" key={ `${item.contractAddress}${item.tokenId}` }>
					<div className="row">
						<div className="col-12 col-sm-3 col-md-2 mb-2">
							<div className="tb-coin">
								<span className="i-coin">
									<img src={ image } alt="" />
								</span>
								<span className="name">{ label }</span>
							</div>
						</div>
						<div className="col-12 col-sm mb-2"><span className="text-break">{ amount }</span></div>
						{/* <div className="col-12 col-sm-3 mb-2 text-right"><span className="text-muted">~ 100 DAI</span></div> */}
					</div>
				</div>
			)
		}

		return null;
	}

	return (
		<div className="db-section">
			<div className="container">
				<div className="c-wrap__table light">
					<div className="item item-header">
						<div className="row">
						<div className="mb-2 col-md-2 text-muted">Token</div>
						<div className="mb-2 col-md text-muted">Amount</div>
						<div className="mb-2 col-md-3 align-right"></div>
						</div>
					</div>
					{
						collateralStat.collaterals
							.sort((item, prev) => { return item.contractAddress.localeCompare(prev.contractAddress) })
							.map((item) => { return getCollateralStatItem(item) })
					}
				</div>
			</div>
		</div>
	)
}