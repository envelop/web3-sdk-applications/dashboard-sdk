
import Header from '../Header';
import Footer from '../Footer';

import {
	ERC20Dispatcher,
	InfoModalDispatcher,
	Web3Dispatcher
} from '../../dispatchers';
import Dashboard from '../Dashboard';
import InfoMessages from '../InfoMessages';

export default function App() {

	return (
		<>
			<InfoModalDispatcher>
			<Web3Dispatcher switchChainCallback={() => { window.location.reload() }}>
			<ERC20Dispatcher>

				<InfoMessages />
				<Header />
				<Dashboard />
				<Footer />

			</ERC20Dispatcher>
			</Web3Dispatcher>
			</InfoModalDispatcher>
		</>
	)

}