
import {
	useContext,
	useEffect,
	useRef,
	useState
} from "react"
import {
	NFT,
	NFTorWNFT,
	BlockedTokenRule,
	WNFT,
	WNFTsStat,
	_AssetType,
	appendWNFTToStat,
	castToWNFT,
	getUserNFTsFromAPI,
	getUserWNFTsFromAPI,
	getWNFTsOfContractFromChain721,
	localStorageGet,
	getBlockedTokens,
	fetchUsersCollectionsFromRegistry,
	getERC721sOfContractFromChain,
	decodeAssetTypeFromString,
	BigNumber,
} from "@envelop/envelop-client-core";

import {
	Web3Context
} from "../../dispatchers";

import {
	TokenRenderType
} from "../NFTCard";

import TokenList from "../TokenList";
import CollateralStat from "../CollateralStat";

import icon_loading from '../../static/pics/loading.svg';

import config from '../../app.config.json';
import { fetchTokenMetadata } from "@envelop/envelop-client-core/dist/fetchtokenwrapper/nft";
import { getUserNFTsFromAlchemy } from "../../utils/alchemy";

export default function Dashboard() {

	const loadTokens = useRef(true);

	const [ currentPage,                   setCurrentPage                   ] = useState<'wrapped' | 'discovered' | 'collateral' | 'sbt'>('discovered');

	const [ tokens,                        setTokens                        ] = useState<{ wrapped: Array<WNFT>, discovered: Array<NFT>, sbt: Array<WNFT>}>({ wrapped: [], discovered: [], sbt: [] });
	const [ wrappedTokensFilteredCount,    setWrappedTokensFilteredCount    ] = useState<number | undefined>(undefined);
	const [ sbtTokensFilteredCount,        setSbtTokensFilteredCount        ] = useState<number | undefined>(undefined);
	const [ discoveredTokensFilteredCount, setDiscoveredTokensFilteredCount ] = useState<number | undefined>(undefined);

	const [ collateralsStat,               setCollateralsStat               ] = useState<WNFTsStat | undefined>(undefined);

	const [ blockedTokens,                 setBlockedTokens                 ] = useState<Array<BlockedTokenRule>>([]);
	const [ doNotBlock,                    setDoNotBlock                    ] = useState(localStorageGet('doNotBlockTokens') !== '' ? JSON.parse(localStorageGet('doNotBlockTokens')) : false);

	const [ tokensLoading,                 setTokensLoading                 ] = useState<Array<string>>([ 'waitForUser' ]);

	const waitForWalletDelay = 2; // s
	setTimeout(() => {
		setTokensLoading((prevState) => {
			return [
				...prevState.filter((item) => { return item !== 'waitForUser' }),
			]
		});
	}, waitForWalletDelay*1000)

	const {
		userAddress,
		currentChainId,
		currentChain,
		getWeb3Force,
	} = useContext(Web3Context);

	const pendingTokens = useRef<Array<string>>([]);
	const updateTokenJSON = (token: NFTorWNFT) => {
		if ( !!token.image && token.image.toLowerCase() !== ('service:loading') ) { return; }
		if ( pendingTokens.current.includes(`${token.contractAddress.toLowerCase()}${token.tokenId.toLowerCase()}`) ) { return; }

		pendingTokens.current = [
			...pendingTokens.current,
			`${token.contractAddress.toLowerCase()}${token.tokenId.toLowerCase()}`
		];

		const setTokenMetadataSuccess = (data: any) => {
			setTokens((prevState) => {
				let output = prevState;
				const foundInWrapped = prevState.wrapped.find((item) => { return item.contractAddress.toLowerCase() === token.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === token.tokenId.toLowerCase() });
				if ( foundInWrapped ) {
					output = {
						wrapped: [
							...output.wrapped.filter((item) => { return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() || item.tokenId.toLowerCase() !== token.tokenId.toLowerCase() }),
							castToWNFT({
								...token,
								...data
							})
						],
						discovered: prevState.discovered,
						sbt: prevState.sbt
					}
				}

				const foundInDiscovered = prevState.discovered.find((item) => { return item.contractAddress.toLowerCase() === token.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === token.tokenId.toLowerCase() });
				if ( foundInDiscovered ) {
					output = {
						discovered: [
							...output.discovered.filter((item) => { return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() || item.tokenId.toLowerCase() !== token.tokenId.toLowerCase() }),
							{
								...token,
								...data
							}
						],
						wrapped: prevState.wrapped,
						sbt: prevState.sbt,
					}
				}

				const foundInSbt = prevState.sbt.find((item) => { return item.contractAddress.toLowerCase() === token.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === token.tokenId.toLowerCase() });
				if ( foundInSbt ) {
					output = {
						sbt: [
							...output.sbt.filter((item) => { return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() || item.tokenId.toLowerCase() !== token.tokenId.toLowerCase() }),
							castToWNFT({
								...token,
								...data
							})
						],
						wrapped: prevState.wrapped,
						discovered: prevState.discovered,
					}
				}

				return output;
			});

			pendingTokens.current = pendingTokens.current.filter((item) => { return item !== `${token.contractAddress.toLowerCase()}${token.tokenId.toLowerCase()}` });
		}
		const setTokenMetadataError = () => {
			setTokens((prevState) => {

				let output = prevState;
				const foundInWrapped = prevState.wrapped.find((item) => { return item.contractAddress.toLowerCase() === token.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === token.tokenId.toLowerCase() });
				if ( foundInWrapped ) {
					output = {
						wrapped: [
							...output.wrapped.filter((item) => { return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() || item.tokenId.toLowerCase() !== token.tokenId.toLowerCase() }),
							castToWNFT({
								...token,
								image: 'service:cannotload'
							})
						],
						discovered: prevState.discovered,
						sbt: prevState.sbt
					}
				}

				const foundInDiscovered = prevState.discovered.find((item) => { return item.contractAddress.toLowerCase() === token.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === token.tokenId.toLowerCase() });
				if ( foundInDiscovered ) {
					output = {
						discovered: [
							...output.discovered.filter((item) => { return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() || item.tokenId.toLowerCase() !== token.tokenId.toLowerCase() }),
							{
								...token,
								image: 'service:cannotload'
							}
						],
						wrapped: prevState.wrapped,
						sbt: prevState.sbt,
					}
				}

				const foundInSbt = prevState.sbt.find((item) => { return item.contractAddress.toLowerCase() === token.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === token.tokenId.toLowerCase() });
				if ( foundInSbt ) {
					output = {
						sbt: [
							...output.sbt.filter((item) => { return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() || item.tokenId.toLowerCase() !== token.tokenId.toLowerCase() }),
							castToWNFT({
								...token,
								image: 'service:cannotload'
							})
						],
						wrapped: prevState.wrapped,
						discovered: prevState.discovered,
					}
				}

				return output;
			});

			pendingTokens.current = pendingTokens.current.filter((item) => { return item !== `${token.contractAddress.toLowerCase()}${token.tokenId.toLowerCase()}` });
		}

		fetchTokenMetadata(token)
			.then((data) => {
				if (
					!data ||
					!data.tokenUrl ||
					!data.image
				) {
					setTokenMetadataError();
					return;
				}

				setTokenMetadataSuccess(data);
			})
			.catch((e) => {
				setTokenMetadataError();
			})
	}

	// fetch tokens
	useEffect(() => {
		const fetchBlockedTokensList = async (cb: Function) => {
			if ( currentChainId === 0 ) { return; }

			const data = await getBlockedTokens( currentChainId, userAddress );
			setBlockedTokens((prevState) => {
				return [
					...prevState.filter((item) => {
						return !data.find((iitem) => {
							return iitem.chain_id === item.chain_id &&
							iitem.contract_address === item.contract_address &&
							iitem.token_id === item.token_id &&
							iitem.base_uri === item.base_uri
						})
					}),
					...data
				]
			})

			cb(data);
		}
		const fetchWNFTs721FromChain = async () => {

			if ( currentChainId === 0 ) { return; }
			if ( !userAddress ) { return; }

			setTokensLoading((prevState) => {
				return [
					...prevState.filter((item) => { return item !== 'fetchWNFTs721FromChain' }),
					'fetchWNFTs721FromChain'
				]
			});

			const foundChainData = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChainId });
			if ( !foundChainData ) { return; }
			let storages = foundChainData.WNFTStorageContracts
				.filter((item) => { return item.assetType === '721' })
				.flatMap((item) => { return item.contractAddress })

			// saft users collections
			if ( foundChainData.usersCollectionsContractsSAFT ) {
				for (let idx = 0; idx < foundChainData.usersCollectionsContractsSAFT.length; idx++) {
					try {
						const coll = await fetchUsersCollectionsFromRegistry(currentChainId, foundChainData.usersCollectionsContractsSAFT[idx], userAddress);
						const collParsed = coll
							.filter((item) => { return item.assetType === _AssetType.ERC721 })
							.flatMap((item) => { return item.contractAddress })

						storages = [
							...storages,
							...collParsed
						]
					} catch (e) { console.log('Cannot load user collections from', foundChainData.usersCollectionsContractsSAFT[idx], e); }
				}
			}

			// requests per contract
			for (let idx = 0; idx < storages.length; idx++) {
				const item = storages[idx];

				try {

					const wnfts = await getWNFTsOfContractFromChain721(currentChainId, item, userAddress);
					const sbtwnfts = wnfts.filter((item) => { return item.rules.noTransfer });
					const nonsbtwnfts = wnfts.filter((item) => { return !item.rules.noTransfer });

					setWrappedTokensFilteredCount(undefined);
					setTokens((prevState) => {
						return {
							wrapped: [
								...prevState.wrapped.filter((item) => {
									return !wnfts.find((iitem) => {
										return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
										item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
									})
								}),
								...nonsbtwnfts,
							],
							sbt: [
								...prevState.sbt.filter((item) => {
									return !wnfts.find((iitem) => {
										return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
										item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
									})
								}),
								...sbtwnfts,
							],
							discovered: prevState.discovered.filter((item) => {
								return !wnfts.find((iitem) => {
									return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
									item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
								})
							})
						}
					});

				} catch(e) { console.log('Cannot load wnfts from', item, e); }

				// delay for limit rps to node
				await new Promise((resolve) => {
					setTimeout( resolve, 500 );
				});

			}

			setTokensLoading((prevState) => {
				return [
					...prevState.filter((item) => { return item !== 'fetchWNFTs721FromChain' }),
				]
			});
		}
		const fetchNFTs721FromChain = async () => {

			if ( currentChainId === 0 ) { return; }
			if ( !userAddress ) { return; }

			setTokensLoading((prevState) => {
				return [
					...prevState.filter((item) => { return item !== 'fetchNFTs721FromChain' }),
					'fetchNFTs721FromChain'
				]
			});

			// fetch from default mint contract
			const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChainId });
			if ( foundChain && foundChain.nftMinterContract721 ) {
				try {
					const nfts = await getERC721sOfContractFromChain(currentChainId, foundChain.nftMinterContract721, userAddress);
					setWrappedTokensFilteredCount(undefined);
					setTokens((prevState) => {
						return {
							wrapped: prevState.wrapped.filter((iiitem) => {
								return !nfts.find((iiiitem) => {
									return iiitem.contractAddress.toLowerCase() === iiiitem.contractAddress.toLowerCase() &&
									iiitem.tokenId.toLowerCase() === iiiitem.tokenId.toLowerCase()
								})
							}),
							discovered: [
								...prevState.discovered.filter((iitem) => {
									return !nfts.find((iiiitem) => {
										return iitem.contractAddress.toLowerCase() === iiiitem.contractAddress.toLowerCase() &&
										iitem.tokenId.toLowerCase() === iiiitem.tokenId.toLowerCase()
									})
								}),
								...nfts,
							],
							sbt: prevState.sbt,
						}
					});


				} catch(e) {
					console.log('Cannot fetch tokens from users collection', foundChain.nftMinterContract721, e);
				}
			}

			// fetch from registries
			if ( foundChain && foundChain.usersCollectionsContracts ) {

				for (let idx = 0; idx < foundChain.usersCollectionsContracts.length; idx++) {
					const item = foundChain.usersCollectionsContracts[idx];

					const coll = await fetchUsersCollectionsFromRegistry(currentChainId, item, userAddress);

					for (let iidx = 0; iidx < coll.length; iidx++) {
						const iitem = coll[iidx];

						if ( iitem.assetType === _AssetType.ERC721 ) {
							try {
								const nfts = await getERC721sOfContractFromChain(currentChainId, iitem.contractAddress, userAddress);
								setWrappedTokensFilteredCount(undefined);

								setTokens((prevState) => {
									return {
										wrapped: prevState.wrapped.filter((iitem) => {
											return !nfts.find((iiiitem) => {
												return iitem.contractAddress.toLowerCase() === iiiitem.contractAddress.toLowerCase() &&
												iitem.tokenId.toLowerCase() === iiiitem.tokenId.toLowerCase()
											})
										}),
										discovered: [
											...prevState.discovered.filter((iitem) => {
												return !nfts.find((iiiitem) => {
													return iitem.contractAddress.toLowerCase() === iiiitem.contractAddress.toLowerCase() &&
													iitem.tokenId.toLowerCase() === iiiitem.tokenId.toLowerCase()
												})
											}),
											...nfts,
										],
										sbt: prevState.sbt,
									}
								});


							} catch(e) {
								console.log('Cannot fetch tokens from users collection', iitem.contractAddress, e);
							}

						}

						// delay for limit rps to node
						await new Promise((resolve) => {
							setTimeout( resolve, 500 );
						});
					}
				}


				// // batch request, less responsive
				// getWNFTsOfContractFromChain721Batch(currentChainId, storages, userAddress)
				// 	.then((data) => {
				// 		setWrappedTokensFilteredCount(undefined);
				// 		setTokens((prevState) => {
				// 			return {
				// 				wrapped: [
				// 					...prevState.wrapped,
				// 					...data,
				// 				],
				// 				discovered: prevState.discovered.filter((item) => {
				// 					return !wnfts.find((iitem) => {
				// 						return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
				// 						item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
				// 					})
				// 				}),
				// 			}
				// 		})
				// 	});


			}

			setTokensLoading((prevState) => {
				return [
					...prevState.filter((item) => { return item !== 'fetchNFTs721FromChain' }),
				]
			});
		}

		const fetchWNFTs721 = async (page: number, blockedTokens?: Array<BlockedTokenRule>) => {
			if ( currentChainId === 0 ) { return; }
			if ( !userAddress ) { return; }

			if ( !currentChain?.hasOracle ) {
				fetchWNFTs721FromChain();
				return;
			}

			if ( page === 1 ) {
				setTokensLoading((prevState) => {
					return [
						...prevState.filter((item) => { return item !== 'fetchWNFTs721' }),
						'fetchWNFTs721'
					]
				});
			}

			const wnfts = await getUserWNFTsFromAPI(currentChainId, _AssetType.ERC721, userAddress, page, { tokensOnPage: 50, doNotFilterBlocked: doNotBlock });
			const sbtwnfts = wnfts.filter((item) => { return item.rules.noTransfer });
			const nonsbtwnfts = wnfts.filter((item) => { return !item.rules.noTransfer });

			setWrappedTokensFilteredCount(undefined);
			setTokens((prevState) => {

				return {
					wrapped: [
						...prevState.wrapped.filter((item) => {
							return !wnfts.find((iitem) => {
								return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
								item.tokenId === iitem.tokenId
							})
						}),
						...nonsbtwnfts,
					],
					sbt: [
						...prevState.sbt.filter((item) => {
							return !wnfts.find((iitem) => {
								return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
								item.tokenId === iitem.tokenId
							})
						}),
						...sbtwnfts,
					],
					discovered: prevState.discovered.filter((item) => {
						return !wnfts.find((iitem) => {
							return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
							item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
						})
					}),
				}
			});

			if ( page === 1 && wnfts.length === 0 ) {
				// chain fallback
				fetchWNFTs721FromChain();
			}
			if ( wnfts.length ) { fetchWNFTs721(page + 1); }

			setTokensLoading((prevState) => {
				return [
					...prevState.filter((item) => { return item !== 'fetchWNFTs721' }),
				]
			});
		}
		const fetchWNFTs1155 = async (page: number, blockedTokens?: Array<BlockedTokenRule>) => {
			if ( currentChainId === 0 ) { return; }
			if ( !userAddress ) { return; }

			if ( !currentChain?.hasOracle ) {
				return;
			}

			if ( page === 1 ) {
				setTokensLoading((prevState) => {
					return [
						...prevState.filter((item) => { return item !== 'fetchWNFTs1155' }),
						'fetchWNFTs1155'
					]
				});
			}

			const wnfts = await getUserWNFTsFromAPI(currentChainId, _AssetType.ERC1155, userAddress, page, { tokensOnPage: 50, doNotFilterBlocked: doNotBlock });
			const sbtwnfts = wnfts.filter((item) => { return item.rules.noTransfer });
			const nonsbtwnfts = wnfts.filter((item) => { return !item.rules.noTransfer });

			setWrappedTokensFilteredCount(undefined);
			setTokens((prevState) => {
				return {
					wrapped: [
						...prevState.wrapped.filter((item) => {
							return !wnfts.find((iitem) => {
								return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
								item.tokenId === iitem.tokenId
							})
						}),
						...nonsbtwnfts,
					],
					sbt: [
						...prevState.sbt.filter((item) => {
							return !wnfts.find((iitem) => {
								return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
								item.tokenId === iitem.tokenId
							})
						}),
						...sbtwnfts,
					],
					discovered: prevState.discovered.filter((item) => {
						return !wnfts.find((iitem) => {
							return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
							item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
						})
					}),
				}
			});

			if ( wnfts.length ) { fetchNFTs1155(page + 1); }
			setTokensLoading((prevState) => {
				return [
					...prevState.filter((item) => { return item !== 'fetchWNFTs1155' }),
				]
			});
		}
		const fetchNFTs721 = async (page: number, blockedTokens?: Array<BlockedTokenRule>) => {
			if ( currentChainId === 0 ) { return; }
			if ( !userAddress ) { return; }

			if ( !currentChain?.hasOracle ) {
				return;
			}

			if (page === 1) {
				setTokensLoading((prevState) => {
					return [
						...prevState.filter((item) => { return item !== 'fetchNFTs721' }),
						'fetchNFTs721'
					]
				});
			}

			const nfts = await getUserNFTsFromAPI(currentChainId, _AssetType.ERC721, userAddress, page, { filterWNFTS: true, tokensOnPage: 100, doNotFilterBlocked: doNotBlock });
			setDiscoveredTokensFilteredCount(undefined);
			setTokens((prevState) => {
				return {
					discovered: [
						...prevState.discovered.filter((item) => {
							return !nfts.find((iitem) => {
								return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
								item.tokenId === iitem.tokenId
							})
						}),
						...nfts,
					],
					wrapped: prevState.wrapped,
					sbt: prevState.sbt,
				}
			});

			if ( nfts.length ) { fetchNFTs721( page+1 ) }
			setTokensLoading((prevState) => {
				return [
					...prevState.filter((item) => { return item !== 'fetchNFTs721' }),
				]
			});
		}
		const fetchNFTs1155 = async (page: number, blockedTokens?: Array<BlockedTokenRule>) => {
			if ( currentChainId === 0 ) { return; }
			if ( !userAddress ) { return; }

			if ( !currentChain?.hasOracle ) {
				return;
			}

			if (page === 1) {
				setTokensLoading((prevState) => {
					return [
						...prevState.filter((item) => { return item !== 'fetchNFTs1155' }),
						'fetchNFTs1155'
					]
				});
			}

			const nfts = await getUserNFTsFromAPI(currentChainId, _AssetType.ERC1155, userAddress, page, { filterWNFTS: true, tokensOnPage: 100, doNotFilterBlocked: doNotBlock });
			setDiscoveredTokensFilteredCount(undefined);
			setTokens((prevState) => {
				return {
					discovered: [
						...prevState.discovered.filter((item) => {
							return !nfts.find((iitem) => {
								return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
								item.tokenId === iitem.tokenId
							})
						}),
						...nfts,
					],
					wrapped: prevState.wrapped,
					sbt: prevState.sbt,
				}
			});

			if ( nfts.length ) { fetchNFTs1155( page+1 ) }
			setTokensLoading((prevState) => {
				return [
					...prevState.filter((item) => { return item !== 'fetchNFTs1155' }),
				]
			});
		}

		const fetchNFTsAlchemy = async (pageKey?: string, blockedTokens?: Array<BlockedTokenRule>) => {
			if ( currentChainId === 0 ) { return; }
			if ( !userAddress ) { return; }
			if ( !loadTokens.current ) { return; }

			setTokensLoading((prevState) => {
				return [
					...prevState.filter((item) => { return item !== 'fetchNFTsAlchemy' }),
					'fetchNFTsAlchemy'
				]
			});

			let nfts: any;
			try {
				nfts = await getUserNFTsFromAlchemy(currentChainId, userAddress, pageKey);
			} catch(e) {
				setTokensLoading((prevState) => {
					return [
						...prevState.filter((item) => { return item !== 'fetchNFTs1155' }),
					]
				});
				console.log('Cannot load nfts from alchemy', e);
				fetchNFTs721FromChain();
				return;
			}
			if ( !nfts.ownedNfts ) {
				setTokensLoading((prevState) => {
					return [
						...prevState.filter((item) => { return item !== 'fetchNFTs1155' }),
					]
				});
				fetchWNFTs721FromChain();
				return;
			}
			const nftsParsed: Array<NFT> = await Promise.all(
				nfts.ownedNfts
				.filter((item: any) => {
					const found = blockedTokens?.find((iitem) => {
						return iitem.contract_address.toLowerCase() === item.contract.address.toLowerCase()
					});
					return !found;
				})
				.map(async (item: any) => {

					const baseData: NFT = {
						assetType: item.contract?.tokenType ? decodeAssetTypeFromString(item.contract.tokenType) : _AssetType.ERC721,
						contractAddress: item.contract?.address || '',
						chainId: currentChainId,
						owner: userAddress,
						tokenId: item.tokenId || '',
						amount: new BigNumber(item.balance) || new BigNumber(0),
						description: item.raw?.metadata?.description || '',
						image: 'service:loading',
						imageRaw: item.raw?.metadata?.image || '',
						name: item.raw?.metadata?.name || '',
						tokenUrl: 'service:loading',
						tokenUrlRaw: item.raw?.tokenUri || '',
						tokenUrlBody: item.raw?.metadata || '',
						blockNumber: item.mint?.blockNumber || '',
						logIndex: '0',
					}

					return baseData;

				})
			);

			setDiscoveredTokensFilteredCount(undefined);
			setTokens((prevState) => {
				if ( !loadTokens.current ) { return prevState; }
				return {
					discovered: [
						...prevState.discovered.filter((item) => {
							return !nftsParsed.find((iitem) => {
								return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() &&
								item.tokenId === iitem.tokenId
							})
						}),
						...nftsParsed,
					],
					wrapped: prevState.wrapped,
					sbt: prevState.sbt,
				}
			});
			setTokensLoading((prevState) => {
				return [
					...prevState.filter((item) => { return item !== 'fetchNFTs1155' }),
				]
			});
			if ( nfts.pageKey ) { setTimeout(() => { fetchNFTsAlchemy( nfts.pageKey, blockedTokens ); }, 1000); }
		}

		setTokens({ wrapped: [], discovered: [], sbt: [] });
		setCollateralsStat(undefined);

		fetchBlockedTokensList((blockedTokens: Array<BlockedTokenRule>) => {
			fetchNFTs721(1, blockedTokens);
			fetchNFTs1155(1, blockedTokens);
			fetchWNFTs721(1, blockedTokens);
			fetchWNFTs1155(1, blockedTokens);
			fetchNFTsAlchemy(undefined, blockedTokens);
		});
	}, [ userAddress, currentChainId, currentChain ])

	// calc stats
	useEffect(() => {
		let outStat: WNFTsStat | undefined;

		tokens.wrapped.forEach((item) => {
			outStat = appendWNFTToStat(castToWNFT(item), outStat);
		})
		if ( outStat ) { setCollateralsStat(outStat) }
	}, [ tokens ])


	// filter discovered tokens by wrapped
	useEffect(() => {
		if ( currentPage === 'wrapped' && !tokens.wrapped.length ) { setCurrentPage('discovered'); }
		if ( currentPage === 'sbt' && !tokens.sbt.length ) { setCurrentPage('discovered'); }
		if ( currentPage === 'collateral' && !collateralsStat ) { setCurrentPage('discovered'); }
		if ( currentPage === 'discovered' && !tokens.discovered.length ) {
			if ( tokens.wrapped.length ) { setCurrentPage('wrapped'); }
			if ( tokens.sbt.length ) { setCurrentPage('sbt'); }
		}
	}, [ tokens, collateralsStat ])

	const getWrappedTokensCount = () => {
		if ( !tokens.wrapped.length ) { return '' }

		if ( wrappedTokensFilteredCount === undefined || tokens.wrapped.length === wrappedTokensFilteredCount ) {
			return ( <span>{ tokens.wrapped.length }</span> )
		}

		return ( <span>{ wrappedTokensFilteredCount } / { tokens.wrapped.length }</span> )
	}
	const getSBTTokensCount = () => {
		if ( !tokens.sbt.length ) { return '' }

		if ( sbtTokensFilteredCount === undefined || tokens.sbt.length === sbtTokensFilteredCount ) {
			return ( <span>{ tokens.sbt.length }</span> )
		}

		return ( <span>{ sbtTokensFilteredCount } / { tokens.sbt.length }</span> )
	}
	const getDiscoveredTokensCount = () => {
		if ( !tokens.discovered.length ) { return '' }

		if ( discoveredTokensFilteredCount === undefined || tokens.discovered.length === discoveredTokensFilteredCount ) {
			return ( <span>{ tokens.discovered.length }</span> )
		}

		return ( <span>{ discoveredTokensFilteredCount } / { tokens.discovered.length }</span> )
	}
	const getCollateralCount = () => {
		if ( !collateralsStat ) { return null; }
		if ( collateralsStat.collaterals.length === 0 ) { return ''; }

		return ( <span>{ collateralsStat.collaterals.length }</span> )
	}

	const getCurrentPage = () => {

		if ( tokensLoading.length && ( !tokens.discovered.length && !tokens.wrapped.length && !tokens.sbt.length ) ) {
			return (
				<div className="db-section">
				<div className="container">
				<div className="lp-list__footer">
					<img className="loading" src={ icon_loading } alt="" />
				</div>
				</div>
				</div>
			)
		}

		if ( !userAddress ) {
			return (
				<div className="db-section">
				<div className="container">
				<div className="row justify-content-center">
					<div className="col-auto">
						<button
							className="btn btn-lg btn-outline"
							onClick={async () => {try { await getWeb3Force(); } catch(e: any) { console.log('Cannot connect', e); } }}
						>
							Connect to&nbsp;your wallet to&nbsp;load&nbsp;tokens
						</button>
					</div>
				</div>
				</div>
				</div>
			)
		}

		if ( !tokens.discovered.length && !tokens.wrapped.length && !tokens.sbt.length ) {
			return (
				<div className="db-section">
				<div className="container">
				<div className="lp-list__footer">
					<div className="nomore">There's no more wNFT yet</div>
				</div>
				</div>
				</div>
			)
		}

		if ( currentPage === 'wrapped' ) {
			return (
				<div className="db-section">
				<div className="container">

				{/* <div>
					<label className="checkbox">
						<input
							type="checkbox"
							checked={ doNotBlock }
							onChange={(e) => {
								setDoNotBlock(e.target.checked);
								localStorageSet('doNotBlockTokens', JSON.stringify(e.target.checked));
								window.location.reload();
							}}
						/>
						<span className="check"></span>
						<span className="check-text">Do not block tokens</span>
					</label>
				</div> */}

				<TokenList
					key={'wrapped'}
					tokens={ tokens.wrapped }
					updateFilteredCount={(count)=>{ setWrappedTokensFilteredCount(count); }}
					tokenRenderType={ TokenRenderType.wrapped }
					onTokenRender={ updateTokenJSON }
					transferSuccessCallback={(token: NFTorWNFT) => {
						setTokens((prevState) => {
							return {
								wrapped: prevState.wrapped.filter((item) => {
									return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() &&
										item.tokenId !== token.tokenId
								}),
								discovered: prevState.discovered,
								sbt: prevState.sbt
							}
						})
					}}
				/>
				</div>
				</div>
			)
		}
		if ( currentPage === 'sbt' ) {
			return (
				<div className="db-section">
				<div className="container">

				{/* <div>
					<label className="checkbox">
						<input
							type="checkbox"
							checked={ doNotBlock }
							onChange={(e) => {
								setDoNotBlock(e.target.checked);
								localStorageSet('doNotBlockTokens', JSON.stringify(e.target.checked));
								window.location.reload();
							}}
						/>
						<span className="check"></span>
						<span className="check-text">Do not block tokens</span>
					</label>
				</div> */}

				<TokenList
					key={'sbt'}
					tokens={ tokens.sbt }
					updateFilteredCount={(count)=>{ setSbtTokensFilteredCount(count); }}
					tokenRenderType={ TokenRenderType.wrapped }
					onTokenRender={ updateTokenJSON }
					transferSuccessCallback={(token: NFTorWNFT) => {
						setTokens((prevState) => {
							return {
								sbt: prevState.sbt.filter((item) => {
									return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() &&
										item.tokenId !== token.tokenId
								}),
								discovered: prevState.discovered,
								wrapped: prevState.wrapped
							}
						})
					}}
				/>
				</div>
				</div>
			)
		}
		if ( currentPage === 'discovered' ) {
			return (
				<div className="db-section">
				<div className="container">

				<div>
					{/* <label className="checkbox">
						<input
							type="checkbox"
							checked={ doNotBlock }
							onChange={(e) => {
								setDoNotBlock(e.target.checked);
								localStorageSet('doNotBlockTokens', JSON.stringify(e.target.checked));
								window.location.reload();
							}}
						/>
						<span className="check"></span>
						<span className="check-text">Do not block tokens</span>
					</label> */}
				</div>

				<TokenList
					key={'discovered'}
					tokens={ tokens.discovered }
					updateFilteredCount={(count)=>{ setDiscoveredTokensFilteredCount(count); }}
					tokenRenderType={ TokenRenderType.discovered }
					onTokenRender={ updateTokenJSON }
					transferSuccessCallback={(token: NFTorWNFT) => {
						setTokens((prevState) => {
							return {
								discovered: prevState.discovered.filter((item) => {
									return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() &&
										item.tokenId !== token.tokenId
								}),
								wrapped: prevState.wrapped,
								sbt: prevState.sbt
							}
						})
					}}
				/>
				</div>
				</div>
			)
		}
		if ( currentPage === 'collateral' && collateralsStat ) {
			return (
				<CollateralStat
					collateralStat={ collateralsStat }
				/>
			)
		}
		return null;
	}
	const getPageSelector = () => {

		// if ( !userAddress ) { return null; }

		return (
			<div className="db-section__toggle">
				{ tokens.discovered.length ?
					(
						<button
							className={ `tab ${ currentPage === 'discovered' ? 'active' : '' }` }
							onClick={() => { setCurrentPage('discovered') }}
						>
							NFT
							{ ' ' }
							{ getDiscoveredTokensCount() }
						</button>
					) : null
				}
				{ tokens.wrapped.length ?
					(
						<button
							className={ `tab ${ currentPage === 'wrapped' ? 'active' : '' }` }
							onClick={() => { setCurrentPage('wrapped') }}
						>
							wNFT
							{ ' ' }
							{ getWrappedTokensCount() }
						</button>
					) : null
				}
				{ tokens.sbt.length ?
					(
						<button
							className={ `tab ${ currentPage === 'sbt' ? 'active' : '' }` }
							onClick={() => { setCurrentPage('sbt') }}
						>
							SBT
							{ ' ' }
							{ getSBTTokensCount() }
						</button>
					) : null
				}
				{
					collateralsStat && collateralsStat.collaterals.length ? (
						<button
							className={ `tab ${ currentPage === 'collateral' ? 'active' : '' }` }
							onClick={() => { setCurrentPage('collateral') }}
						>
							Collateral
							{ ' ' }
							{ getCollateralCount() }
						</button>
					) : null
				}
			</div>
		)
	}

	const getInfoText = () => {
		return (
			<div className="db-info">
				<div className="divider right"></div>
				<div className="container mt-6">
					<div className="row">
						<div className="col-12 col-md-8">
							<div className="c-wrap">
								<p><b>NFT 2.0&nbsp;is known under many names</b>: Smart, Wrapped, Programmable, etc. </p>
								<p><b>DAO Envelop (2020) is&nbsp;the pioneer of&nbsp;this trend.</b><br /> You can do&nbsp;a&nbsp;lot with Envelop applications: create wrapped NFT, collateralize ERC-20 tokens and native coins (and NFTs in&nbsp;some special cases), and use complex mechanics like Cross NFT, which are extending to&nbsp;ZKP very soon (2023).</p>
							</div>
						</div>
						<div className="col-12 col-md-4">
							<div className="text-small">
								<p>If&nbsp;you have constructive suggestions on&nbsp;how to&nbsp;improve Envelop applications, you are welcome to&nbsp;participate in&nbsp;the bounty campaign. Envelop administrator is&nbsp;always in&nbsp;touch. Write and develop a&nbsp;new market with&nbsp;us!</p><a className="btn btn-sm btn-border" href="https://t.me/TW4Ys" target="_blank" rel="noopener noreferrer">Write to Envelop</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

	return (
		<main className="s-main">

			<div className="container mt-4">
				<div className="row mb-6">
						<div className="col-12 col-sm-4 col-md-3 order-sm-2 mb-4 mb-sm-0">
							<a className="btn btn-grad w-100" href="/wrap">
								Free Wrap
							</a>
						</div>

						<div className="col-12 col-md order-md-1">
							{ getPageSelector() }
						</div>
				</div>

			</div>

			{ getCurrentPage() }

			{ getInfoText() }

		</main>
	)
}