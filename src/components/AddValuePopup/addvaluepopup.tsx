import React, {
	useContext,
	useEffect,
	useState
} from 'react';

import TippyWrapper from '../TippyWrapper';
import CoinSelector from '../CoinSelector';

import CollateralViewer, {
	CollateralError
} from '../CollateralViewer';

import {
	BigNumber,
	ChainType,
	CollateralItem,
	ERC20Type,
	LockType,
	WNFT,
	Web3,
	_AssetType,
	addThousandSeparator,
	addValueToWNFT,
	addValueToWNFTMultisig,
	checkApprovalERC1155,
	checkApprovalERC721,
	combineURLs,
	compactString,
	getChainId,
	getERC20BalanceFromChain,
	getMaxCollateralSlots,
	getNFTById,
	getNullERC20,
	getWNFTById,
	getWNFTWrapperContract,
	getWrapperTechToken,
	localStorageGet,
	makeERC20Allowance,
	makeERC20AllowanceMultisig,
	removeThousandSeparator,
	setApprovalERC1155,
	setApprovalERC1155Multisig,
	setApprovalERC721,
	setApprovalERC721Multisig,
	tokenToFloat,
	tokenToInt,
	getWhitelist,
	getWrapperWhitelist,
	isEnabledForCollateral,
	isEnabledForFee,
	isEnabledRemoveFromCollateral,
} from '@envelop/envelop-client-core';
import {
	AdvancedLoaderStageType,
	ERC20Context,
	InfoModalContext,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes
} from '../../dispatchers';

import config                from '../../app.config.json';

import icon_attention        from '../../static/pics/icons/i-attention.svg';
import icon_external         from '../../static/pics/icons/i-external.svg';
import default_icon          from '../../static/pics/coins/_default.svg';

type AddValuePopupProps = {
	token          : WNFT,
	closePopup     : Function,
}

export default function AddValuePopup(props: AddValuePopupProps) {

	const {
		token,
		closePopup,
	} = props;

	const niftsyTokenIcon = 'https://envelop.is/assets/img/niftsy.svg';
	const v0AppLink       = 'https://appv0.envelop.is';

	const {
		userAddress,
		currentChain,
		balanceNative,
		web3,
		getWeb3Force,
	} = useContext(Web3Context);
	const {
		erc20List,
		requestERC20Token
	} = useContext(ERC20Context);
	const {
		setModal,
		setLoading,
		unsetModal,
		createAdvancedLoader,
		updateStepAdvancedLoader,
		setError,
	} = useContext(InfoModalContext);

	const [ enabledForFee,               setEnabledForFee               ] = useState<Array<string>>([]);
	const [ enabledForCollateral,        setEnabledForCollateral        ] = useState<Array<string>>([]);
	const [ enabledRemoveFromCollateral, setEnabledRemoveFromCollateral ] = useState<Array<string>>([]);
	const [ storageContracts,            setStorageContracts            ] = useState<Array<string>>([]);
	const [ techToken,                   setTechToken                   ] = useState('');

	const [ wrapperContract,             setWrapperContract             ] = useState<string>('');
	const [ whitelistContract,           setWhitelistContract           ] = useState<string | undefined>(undefined);
	const [ collateralPopupShown,        setCollateralPopupShown        ] = useState<boolean>(false);
	const [ collaterals,                 setCollaterals                 ] = useState<Array<CollateralItem>>([]);
	const [ inputCollateralAmount,       setInputCollateralAmount       ] = useState('');
	const [ inputCollateralAssetType,    setInputCollateralAssetType    ] = useState<_AssetType>(_AssetType.native);
	const [ inputCollateralAddress,      setInputCollateralAddress      ] = useState('');
	const [ inputCollateralTokenId,      setInputCollateralTokenId      ] = useState('');
	const [ collateralErrors,            setCollateralErrors            ] = useState<Array<CollateralError>>([]);
	const [ maxCollaterals,              setMaxCollaterals              ] = useState<number>(25);

	useEffect(() => {
		const getWrapperParams = async () => {
			if ( !currentChain ) { return; }

			const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChain.chainId });
			if ( !foundChain ) {
				setError('Unsupported chain');
				return;
			}
			const _storageContracts = foundChain.WNFTStorageContracts.map((item) => { return item.contractAddress });
			setStorageContracts(_storageContracts);

			const _wrapperContract = await getWNFTWrapperContract(currentChain.chainId, token.contractAddress);
			setWrapperContract(_wrapperContract);

			const _techToken = await getWrapperTechToken(currentChain.chainId, _wrapperContract);
			setTechToken(_techToken);

			const foundLock = token.locks.find((item) => { return item.lockType === LockType.slots });
			if ( foundLock ) {
				setMaxCollaterals(foundLock.param.toNumber());
			} else {
				const wrapperMax = await getMaxCollateralSlots(currentChain.chainId, _wrapperContract);
				if ( wrapperMax ) {
					setMaxCollaterals(wrapperMax);
				} else {
					setMaxCollaterals(25);
				}
			}

			const _whitelistContract = await getWrapperWhitelist(currentChain.chainId, _wrapperContract);
			if ( !_whitelistContract ) {
				setWhitelistContract(undefined);
				setEnabledForCollateral([]);
				setEnabledForFee([]);
				setEnabledRemoveFromCollateral([]);
				return;
			}

			const _whitelist = await getWhitelist(currentChain.chainId, _whitelistContract);
			await Promise.all(_whitelist.map(async (item) => {
				if ( item.assetType === _AssetType.ERC20 ) {
					requestERC20Token(item.contractAddress, userAddress);
				}
				return item.contractAddress;
			}));
			for (const idx in _whitelist) {
				if (Object.prototype.hasOwnProperty.call(_whitelist, idx)) {
					const item = _whitelist[idx];

					if ( item.assetType !== _AssetType.ERC20 ) { return; }

					// delay for limit rps to node
					await new Promise((resolve) => {
						setTimeout( resolve, 500 );
					});

					if ( await isEnabledForCollateral(currentChain.chainId, _whitelistContract, item.contractAddress) ) {
						setEnabledForCollateral((prevState) => {
							return [
								...prevState.filter((iitem) => { return iitem.toLowerCase() !== item.contractAddress.toLowerCase() }),
								item.contractAddress,
							]
						});
					}
					if ( await isEnabledForFee(currentChain.chainId, _whitelistContract, item.contractAddress) ) {
						setEnabledForFee((prevState) => {
							return [
								...prevState.filter((iitem) => { return iitem.toLowerCase() !== item.contractAddress.toLowerCase() }),
								item.contractAddress,
							]
						});
					}
					if ( await isEnabledRemoveFromCollateral(currentChain.chainId, _whitelistContract, item.contractAddress) ) {
						setEnabledRemoveFromCollateral((prevState) => {
							return [
								...prevState.filter((iitem) => { return iitem.toLowerCase() !== item.contractAddress.toLowerCase() }),
								item.contractAddress,
							]
						});
					}
				}
			}
		}

		getWrapperParams();

	}, [ currentChain, userAddress ]);

	const filterERC20ContractByPermissions = (permissions: {
		enabledForCollateral?        : boolean,
		enabledForFee?               : boolean,
		enabledRemoveFromCollateral? : boolean,
	}): Array<ERC20Type> => {
		if ( whitelistContract === undefined ) { return erc20List; }

		// OR LOGIC
		return erc20List.filter((item) => {
			if ( permissions.enabledForCollateral        && enabledForCollateral.find(       (iitem => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() })) ) { return true; }
			if ( permissions.enabledForFee               && enabledForFee.find(              (iitem => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() })) ) { return true; }
			if ( permissions.enabledRemoveFromCollateral && enabledRemoveFromCollateral.find((iitem => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() })) ) { return true; }

			return false;
		})
	}

	// ----- COLLATERAL -----
	// ----- COLLATERAL NATIVE -----
	const isNativeBalanceEnough = () => {
		const amountParsed = new BigNumber(inputCollateralAmount);

		if ( !amountParsed || amountParsed.isNaN() || amountParsed.eq(0) ) { return true; }
		if ( tokenToInt(amountParsed, currentChain?.decimals || 18).lte(balanceNative) ) { return true; }

		return false;
	}
	const isCollateralNativeBtnDisabled = () => {
		if ( inputCollateralAmount === '' ) { return true; }

		const amountParsed = new BigNumber(inputCollateralAmount);
		if ( !amountParsed || amountParsed.isNaN() || amountParsed.eq(0) ) { return true; }

		if ( token.rules.noAddCollateral ) { return true; }

		if ( !isNativeBalanceEnough() ) { return true; }

		const foundNativeCollateral = collaterals.find((item: CollateralItem) => { return item.assetType === _AssetType.native });
		if ( foundNativeCollateral ) { return false; }

		if ( isMaxCollaterals() || overMaxCollaterals() ) { return true; }

		return false;
	}
	const addCollateralNativeRow = () => {

		if ( !collateralPopupShown ) {
			if ( token.rules.noUnwrap ) {
				setModal({
					type: _ModalTypes.info,
					title: 'Warning',
					text: [{ text: 'You can lock collateral forever with these settings', clazz: 'text-orange' }]
				});
				setCollateralPopupShown(true);
			}
		}

		let amountNative = new BigNumber(0);
		const nativeAdded = collaterals.filter((item) => { return item.assetType === _AssetType.native });
		if ( nativeAdded.length && nativeAdded[0].amount ) {
			amountNative = new BigNumber(nativeAdded[0].amount);
		}
		const collateralsUpdated = [
			...collaterals.filter((item) => { return item.assetType !== _AssetType.native }),
			{
				assetType: _AssetType.native,
				contractAddress: '0x0000000000000000000000000000000000000000',
				amount: tokenToInt(new BigNumber(inputCollateralAmount), currentChain?.decimals || 18).plus(amountNative),
			}
		];
		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
	}
	const getAddCollateralNativeBtn = () => {
		return (
			<button
				className="btn btn-grad"
				disabled={ isCollateralNativeBtnDisabled() }
				onClick={() => { addCollateralNativeRow() }}
			>Add</button>
		)
	}
	const getCollateralNativeBalance = () => {
		const getAmount = () => {
			const DECIMALS_TO_SHOW = 5;

			const amount = tokenToFloat(balanceNative, currentChain?.decimals || 18);

			if ( amount.eq(0) ) {
				return (
					<button>0</button>
				)
			}

			if ( amount.lt(10**-DECIMALS_TO_SHOW) ) {
				return (
					<TippyWrapper msg={ amount.toString() }>
						<button
							onClick={() => { setInputCollateralAmount(amount.toString()) }}
						>&lt;{ new BigNumber(10**-DECIMALS_TO_SHOW).toFixed(DECIMALS_TO_SHOW) }</button>
					</TippyWrapper>
				)
			}
			return (
				<button
					onClick={() => { setInputCollateralAmount(amount.toString()) }}
				>{ amount.toFixed(DECIMALS_TO_SHOW) }</button>
			)
		}

		return (
			<div className="c-add__max mt-3">
				<div>
					<span>Max: </span>
					{ getAmount() }
				</div>
			</div>
		)
	}
	const getNativeAmountInput = () => {
		if ( isNativeBalanceEnough() ) {
			return (
				<React.Fragment>
					<div className="select-group">
						<input
							className="input-control"
							type="text"
							placeholder="0.000"
							value={ addThousandSeparator(inputCollateralAmount) }
							onChange={(e) => {
								let value = removeThousandSeparator(e.target.value);
								if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
									if ( new BigNumber(value).isNaN() ) { return; }
									value = new BigNumber(value).toString();
								}
								setInputCollateralAmount(value);
							}}
							onKeyPress={(e) => {
								if ( e.defaultPrevented)                 { return; }
								if ( !!isCollateralNativeBtnDisabled() ) { return; }
								if ( e.key !== 'Enter' )                 { return; }

								addCollateralNativeRow()
							}}
						/>
						<div className="select-coin">
							<div className="select-coin__value">
								<span className="field-unit">
									<span className="i-coin">
										<img src={ currentChain?.tokenIcon || default_icon } alt="" />
									</span>
									{ currentChain?.symbol || compactString('0x0000000000000000000000000000000000000000') }
								</span>
							</div>
						</div>
					</div>
				</React.Fragment>
			)
		} else {
			return (
				<React.Fragment>
					<div className="select-group">
						<input
							className="input-control has-error"
							type="text"
							placeholder="0.000"
							value={ addThousandSeparator(inputCollateralAmount) }
							onChange={(e) => {
								let value = removeThousandSeparator(e.target.value);
								if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
									if ( new BigNumber(value).isNaN() ) { return; }
									value = new BigNumber(value).toString();
								}
								setInputCollateralAmount(value);
							}}
							onKeyPress={(e) => {
								if ( e.defaultPrevented)                 { return; }
								if ( !!isCollateralNativeBtnDisabled() ) { return; }
								if ( e.key !== 'Enter' )                 { return; }

								addCollateralNativeRow()
							}}
						/>
						<div className="select-coin">
							<div className="select-coin__value">
								<span className="field-unit">
									<span className="i-coin">
										<img src={ currentChain?.tokenIcon || default_icon } alt="" />
									</span>
									{ currentChain?.symbol || compactString('0x0000000000000000000000000000000000000000') }
								</span>
							</div>
						</div>
					</div>
					<div className="input-error">Not enough balance</div>
				</React.Fragment>
			)
		}
	}
	const getCollateralNativeBlock = () => {
		if ( inputCollateralAssetType === _AssetType.native ) {
			return (
				<div className="row">
					<div className="col col-12 col-md-7">
						<label className="input-label">Amount</label>
						{ getNativeAmountInput() }

						{ getCollateralNativeBalance() }
					</div>
					<div className="col col-12 col-md-2">
						<label className="input-label">&nbsp;</label>
						{ getAddCollateralNativeBtn() }
					</div>
				</div>
			)
		}
	}
	// ----- END COLLATERAL NATIVE -----

	// ----- COLLATERAL ERC20 -----
	const isERC20BalanceEnough = () => {
		if ( !userAddress ) { return true; }
		if ( inputCollateralAddress.toLowerCase() === techToken.toLowerCase() ) { return true; }
		const amountParsed = new BigNumber(inputCollateralAmount);
		const foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		if ( foundToken && foundToken.balance.lt(tokenToInt(amountParsed, foundToken.decimals || 18)) ) { return false; }

		return true;
	}
	const isCollateralERC20BtnDisabled = () => {
		if ( inputCollateralAddress === '' ) { return true; }
		if ( inputCollateralAmount  === '' ) { return true; }

		const amountParsed = new BigNumber(inputCollateralAmount);
		if ( !amountParsed || amountParsed.isNaN() || amountParsed.eq(0) ) { return true; }

		if ( token.rules.noAddCollateral ) { return true; }

		if ( !isCollateralAddressInWhitelist()   ) { return true; }
		if ( !isERC20BalanceEnough()             ) { return true; }

		const foundERC20Collateral = collaterals.find((item: CollateralItem) => { return item.assetType === _AssetType.ERC20 && item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		if ( !foundERC20Collateral ) {
			if ( isMaxCollaterals() || overMaxCollaterals() ) { return true; }
		}

		return false;
	}
	const addCollateralERC20Row = () => {

		if ( !collateralPopupShown ) {
			if ( token.rules.noUnwrap ) {
				setModal({
					type: _ModalTypes.info,
					title: 'Warning',
					text: [{ text: 'You can lock collateral forever with these settings', clazz: 'text-orange' }]
				});
				setCollateralPopupShown(true);
			}
		}

		// empty means 0-amount collateral slot
		// in case of checked the wNFT royalty recipient
		// for collecting royalty

		let amountAdded = new BigNumber(0);
		let amountToAdd = new BigNumber(inputCollateralAmount);
		let addressToAdd = inputCollateralAddress;

		const erc20Added = collaterals.filter((item) => {
			if ( !item.contractAddress ) { return false; }
			return item.contractAddress.toLowerCase() === addressToAdd.toLowerCase()
		});
		if ( erc20Added.length && erc20Added[0].amount ) {
			amountAdded = new BigNumber(erc20Added[0].amount);
		}

		let tokenToAdd = undefined;

		const foundToken = erc20List.filter((item) => {
			if ( !item.contractAddress ) { return false; }
			return item.contractAddress.toLowerCase() === addressToAdd.toLowerCase()
		});
		if ( foundToken.length ) {
			tokenToAdd = foundToken[0];
		}

		if ( tokenToAdd ) {
			amountAdded = tokenToFloat(new BigNumber(amountAdded), tokenToAdd.decimals || 18);
			amountToAdd = tokenToInt(amountToAdd.plus(amountAdded), tokenToAdd.decimals || 18);
		} else {
			amountToAdd = amountToAdd.plus(amountAdded)
		}

		let collateralsUpdated = collaterals;
		collateralsUpdated = [
			...collateralsUpdated.filter((item) => {
				if ( !item.contractAddress ) { return false; }
				return item.contractAddress.toLowerCase() !== addressToAdd.toLowerCase()
			}),
			{
				assetType: _AssetType.ERC20,
				contractAddress: addressToAdd,
				amount: amountToAdd,
			}
		];

		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
		setInputCollateralAddress('');

	}
	const getAddCollateralERC20Btn = () => {
		return (
			<button
				className="btn btn-grad"
				disabled={ !!isCollateralERC20BtnDisabled() }
				onClick={() => { addCollateralERC20Row() }}
			>Add</button>
		)
	}
	const getCollateralERC20AmountTitle = () => {
		const foundERC20 = erc20List.filter((item) => {
			if ( !item.contractAddress ) { return false; }
			return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase()
		});
		if (
			inputCollateralAddress &&
			inputCollateralAddress !== '' &&
			inputCollateralAddress !== '0' &&
			inputCollateralAddress !== '0x0000000000000000000000000000000000000000' &&
			!foundERC20.length
		) {
			return (
				<TippyWrapper
					msg="Cannot get decimals from contract, enter amount in wei"
				>
					<label className="input-label text-orange">Amount*</label>
				</TippyWrapper>
			)
		} else {
			return (
				<label className="input-label">
					Amount
					<TippyWrapper
						msg="Maximum and allowanced amount of tokens which you can add to collateral of wrapped nft"
					></TippyWrapper>
				</label>
			)
		}
		// return ( <label className="input-label">{ t('Amount') }</label> )
	}
	const getCollateralERC20Balance = () => {
		if ( !userAddress ) { return null; }
		if ( inputCollateralAddress.toLowerCase() === techToken.toLowerCase() ) { return null; }
		if ( inputCollateralAddress === '' ) { return null; }

		const foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		if ( !foundToken ) { return null; }

		const getAmount = () => {
			const DECIMALS_TO_SHOW = 5;

			const amount = tokenToFloat(foundToken.balance, foundToken.decimals || 18);

			if ( amount.eq(0) ) {
				return (
					<button>0</button>
				)
			}

			if ( amount.lt(10**-DECIMALS_TO_SHOW) ) {
				return (
					<TippyWrapper msg={ amount.toString() }>
						<button
							onClick={() => { setInputCollateralAmount(amount.toString()) }}
						>&lt;{ new BigNumber(10**-DECIMALS_TO_SHOW).toFixed(DECIMALS_TO_SHOW) }</button>
					</TippyWrapper>
				)
			}
			return (
				<button
					onClick={() => { setInputCollateralAmount(amount.toString()) }}
				>{ amount.toFixed(DECIMALS_TO_SHOW) }</button>
			)
		}

		return (
			<div className="c-add__max mt-2 mb-0">
				<div className="mt-1 mb-1">
					<span>Max: </span>
					{ getAmount() }
				</div>
				{/* <div>
					<span>Allowance: </span>
					<button
						onClick={() => { setInputCollateralAmount(tokenToFloat(foundToken.allowance, foundToken.decimals || 18).toString()) }}
					>{ tokenToFloat(foundToken.allowance, foundToken.decimals || 18).toString() }</button>
				</div> */}
			</div>
		)
	}
	const getCollateralERC20AddressInput = () => {
		return (
			<React.Fragment>
				<div className="select-group">
					<input
						className={`input-control ${!isCollateralAddressInWhitelist() ? 'has-error' : ''}`}
						type="text"
						placeholder="0.000"
						value={ inputCollateralAddress }
						onChange={(e) => {
							const value = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
							setInputCollateralAddress(value);
							if ( Web3.utils.isAddress(value) ) {
								const foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === value.toLowerCase() });
								if ( !foundToken || foundToken.balance.eq(0) ) { requestERC20Token(value, userAddress); }
							}
						}}
						onKeyPress={(e) => {
							if ( e.defaultPrevented )               { return; }
							if ( !!isCollateralERC20BtnDisabled() ) { return; }
							if ( e.key !== 'Enter' )                { return; }

							addCollateralERC20Row()
						}}
					/>
					<CoinSelector
						tokens        = { filterERC20ContractByPermissions({ enabledForCollateral: true }) }
						selectedToken = { inputCollateralAddress }
						onChange      = {(address: string) => {
							setInputCollateralAddress(address);
							if ( Web3.utils.isAddress(address) ) {
								const foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === address.toLowerCase() });
								if ( !foundToken || foundToken.balance.eq(0) ) { requestERC20Token(address, userAddress); }
							}
						}}
					/>
				</div>
				{
					!isCollateralAddressInWhitelist() ?
					( <div className="input-warning">Please use the <a href="/saft" target="_blank" rel="noopener noreferrer">SAFT dApp</a> if you want to add non-whitelisted tokens to your collateral.</div> ) : null
				}
			</React.Fragment>
		)
	}
	const getCollateralERC20AmountInput = () => {
		if ( !isERC20BalanceEnough() ) {
			return (
				<React.Fragment>
					<input
						className="input-control has-error"
						type="text"
						placeholder=""
						value={ addThousandSeparator(inputCollateralAmount) }
						onChange={(e) => {
							let value = removeThousandSeparator(e.target.value);
							if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
								if ( new BigNumber(value).isNaN() ) { return; }
								value = new BigNumber(value).toString();
							}
							setInputCollateralAmount(value);
						}}
						onKeyPress={(e) => {
							if ( e.defaultPrevented)                     { return; }
							if ( !!isCollateralERC20BtnDisabled() ) { return; }
							if ( e.key !== 'Enter' )                     { return; }

							addCollateralERC20Row()
						}}
					/>
					<div className="input-error">Not enough balance</div>
				</React.Fragment>
			)
		}

		return (
			<React.Fragment>
				<input
					className="input-control"
					type="text"
					placeholder=""
					value={ addThousandSeparator(inputCollateralAmount) }
					onChange={(e) => {
						let value = removeThousandSeparator(e.target.value);
						if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
							if ( new BigNumber(value).isNaN() ) { return; }
							value = new BigNumber(value).toString();
						}
						setInputCollateralAmount(value);
					}}
					onKeyPress={(e) => {
						if ( e.defaultPrevented)                     { return; }
						if ( !!isCollateralERC20BtnDisabled() ) { return; }
						if ( e.key !== 'Enter' )                     { return; }

						addCollateralERC20Row()
					}}
				/>
			</React.Fragment>
		)

	}
	const getERC20Link = () => {
		if ( !currentChain ) { return null; }

		if ( inputCollateralAddress === '' ) { return null; }
		const foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });

		if ( !foundToken ) { return null; }

		let tokenImage = '';
		if ( foundToken.name.toLowerCase() === 'niftsy' ) {
			tokenImage = niftsyTokenIcon
		}

		return (

			<div className="d-flex align-items-center flex-wrap">
				<a
					className="ex-link mr-3 mt-3 mb-3"
					target="_blank"
					rel="noopener noreferrer"
					href={ combineURLs(currentChain.explorerBaseUrl, `/token/${foundToken.contractAddress}`) }
				>
					<small>More about { foundToken.symbol }</small>
					<img className="i-ex" src={ icon_external } alt="" />
				</a>
				<a
					className="btn btn-sm btn-gray"
					href="/"
					onClick={(e) => {
						e.preventDefault();
						if ( !foundToken ) { return; }
						try {
							if ( !(window as any).ethereum ) { return; }
							// TODO
							(window as any).ethereum.request({
								method: 'wallet_watchAsset',
								params: {
									type: 'ERC20', // Initially only supports ERC20, but eventually more!
									options: {
										address: foundToken.contractAddress, // The address that the token is at.
										symbol: foundToken.symbol, // A ticker symbol or shorthand, up to 5 chars.
										decimals: foundToken.decimals, // The number of decimals in the token
										image: tokenImage, // A string url of the token logo
									},
								},
							});
						} catch(e) {
							console.log(e)
						}
					}}
				>Add to Metamask</a>
			</div>
		)
	}
	const getCollateralERC20Block = () => {
		if ( inputCollateralAssetType === _AssetType.ERC20 ) {
			return (
				<div className="row">
					<div className="col col-12 col-md-7">
						<label className="input-label">Token Address</label>
						{ getCollateralERC20AddressInput() }
						{ getERC20Link() }

					</div>
					<div className="col col-12 col-md-3">
						{ getCollateralERC20AmountTitle() }
						{ getCollateralERC20AmountInput() }
						{ getCollateralERC20Balance() }
					</div>
					<div className="col col-12 col-md-2">
						<label className="input-label">&nbsp;</label>
						{ getAddCollateralERC20Btn() }
					</div>
				</div>
			)
		}
	}
	// ----- END COLLATERAL ERC20 -----

	// ----- COLLATERAL ERC721 -----
	const isCollateralERC721BtnDisabled = () => {
		if ( inputCollateralAddress === '' ) { return true; }
		if ( inputCollateralTokenId === '' ) { return true; }

		if ( token.rules.noAddCollateral ) { return true; }

		if ( !isCollateralAddressInWhitelist()   ) { return true; }

		if ( isMaxCollaterals() || overMaxCollaterals() ) { return true; }

		if (
			inputCollateralAddress.toLowerCase() === token.contractAddress.toLowerCase() &&
			inputCollateralTokenId.toLowerCase() === token.tokenId.toLowerCase()
		) { return true; }

		return false;
	}
	const addCollateralERC721Row = () => {

		if ( !collateralPopupShown ) {
			if ( token.rules.noUnwrap ) {
				setModal({
					type: _ModalTypes.info,
					title: 'Warning',
					text: [{ text: 'You can lock collateral forever with these settings', clazz: 'text-orange' }]
				});
				setCollateralPopupShown(true);
			}
		}

		const address = inputCollateralAddress;
		const tokenId = inputCollateralTokenId;

		const collateralsUpdated = [
			...collaterals.filter((item) => {
					if ( item.assetType !== _AssetType.ERC721 ) { return true; }
					if ( !item.contractAddress ) { return false; }
					return item.contractAddress.toLowerCase() !== address.toLowerCase() ||
						item.tokenId !== tokenId
				}),
			{
				assetType: _AssetType.ERC721,
				contractAddress: address,
				tokenId: tokenId,
			}
		];

		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
		setInputCollateralAddress('');
		setInputCollateralTokenId('');

		if ( !currentChain ) { return; }
		if ( !userAddress  ) { return; }
		getNFTById(currentChain.chainId, address, tokenId, userAddress)
			.then((data) => {
				if ( !data ){ return; }

				const collateralsUpdated = [
					...collaterals.filter((item) => {
						if ( item.assetType !== _AssetType.ERC721 ) { return true; }
						if ( !item.contractAddress ) { return false; }
						return item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId !== tokenId
					}),
					{
						assetType: _AssetType.ERC721,
						contractAddress: address,
						tokenId: tokenId,
						tokenImg: data.image || ''
					}
				];

				let collateralErrorsUpdated = collateralErrors;
				if ( data.owner && data.owner.toLowerCase() !== userAddress.toLowerCase() ) {
					collateralErrorsUpdated = [
						...collateralErrorsUpdated,
						{
							contractAddress: address,
							tokenId: tokenId,
							msg: 'Not yours'
						}
					]
				}

				setCollaterals(collateralsUpdated);
				setCollateralErrors(collateralErrorsUpdated);

			})
			.catch((error) => {
				console.log('Cannot fetch 721 token', error);
				const collateralsUpdated = [
					...collaterals.filter((item) => {
						if ( item.assetType !== _AssetType.ERC721 ) { return true; }
						if ( !item.contractAddress ) { return false; }
						return item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId !== tokenId
					}),
					{
						assetType: _AssetType.ERC721,
						contractAddress: address,
						tokenId: tokenId,
						tokenImg: ''
					}
				];

				let collateralErrorsUpdated = [
					...collateralErrors,
					{
						contractAddress: address,
						tokenId: tokenId,
						msg: 'Cannot fetch token'
					}
				]

				setCollaterals(collateralsUpdated);
				setCollateralErrors(collateralErrorsUpdated);
			})
	}
	const getAddCollateralERC721Btn = () => {
		return (
			<button
				className="btn btn-grad"
				disabled={ isCollateralERC721BtnDisabled() }
				onClick={() => { addCollateralERC721Row() }}
			>Add</button>
		)
	}
	const getCollateralERC721AddressInput = () => {
		if ( !isCollateralAddressInWhitelist() ) {
			return (
				<React.Fragment>
					<input
						className="input-control"
						type="text"
						placeholder="Paste here"
						value={ inputCollateralAddress }
							onChange={(e) => { setInputCollateralAddress( e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "") ) }}
							onKeyPress={(e) => {
								if ( e.defaultPrevented)                     { return; }
								if ( !!isCollateralERC20BtnDisabled() ) { return; }
								if ( e.key !== 'Enter' )                     { return; }

								addCollateralERC721Row()
							}}
					/>
					<div className="input-error">Address is not in whitelist</div>
				</React.Fragment>
			)
		}
		if (
			inputCollateralAddress.toLowerCase() === token.contractAddress.toLowerCase() &&
			inputCollateralTokenId.toLowerCase() === token.tokenId.toLowerCase()
		) {
			return (
				<React.Fragment>
					<input
						className="input-control"
						type="text"
						placeholder="Paste here"
						value={ inputCollateralAddress }
							onChange={(e) => { setInputCollateralAddress( e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "") ) }}
							onKeyPress={(e) => {
								if ( e.defaultPrevented)                     { return; }
								if ( !!isCollateralERC20BtnDisabled() ) { return; }
								if ( e.key !== 'Enter' )                     { return; }

								addCollateralERC721Row()
							}}
					/>
					<div className="input-error">Cannot add token to it's collateral</div>
				</React.Fragment>
			)
		}

		return (
			<React.Fragment>
				<input
					className="input-control"
					type="text"
					placeholder="Paste here"
					value={ inputCollateralAddress }
						onChange={(e) => { setInputCollateralAddress( e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "") ) }}
						onKeyPress={(e) => {
							if ( e.defaultPrevented)                     { return; }
							if ( !!isCollateralERC20BtnDisabled() ) { return; }
							if ( e.key !== 'Enter' )                     { return; }

							addCollateralERC721Row()
						}}
				/>
			</React.Fragment>
		)

	}
	const getCollateralERC721Block = () => {
		if ( inputCollateralAssetType === _AssetType.ERC721 ) {
			return (
				<div className="row">
					<div className="col col-12 col-md-7">
						<label className="input-label">NFT Address</label>
						{ getCollateralERC721AddressInput() }
					</div>
					<div className="col col-12 col-md-3">
						<label className="input-label">Token ID</label>
						<input
							className="input-control"
							type="text"
							placeholder="99 900"
							value={ addThousandSeparator(inputCollateralTokenId) }
							onChange={(e) => { setInputCollateralTokenId( e.target.value.toLowerCase().replace(/[^0-9]/g, "") ) }}
							onKeyPress={(e) => {
								if ( e.defaultPrevented)                     { return; }
								if ( !!isCollateralERC20BtnDisabled() ) { return; }
								if ( e.key !== 'Enter' )                     { return; }

								addCollateralERC721Row()
							}}
						/>
					</div>
					<div className="col col-12 col-md-2">
						<label className="input-label">&nbsp;</label>
						{ getAddCollateralERC721Btn() }
					</div>
				</div>
			)
		}
	}
	// ----- END COLLATERAL ERC721 -----

	// ----- COLLATERAL ERC1155 -----
	const isCollateralERC1155BtnDisabled = () => {
		if ( inputCollateralAddress === '' ) { return true; }
		if ( inputCollateralTokenId === '' ) { return true; }

		if ( token.rules.noAddCollateral ) { return true; }

		const amountParsed = new BigNumber(inputCollateralAmount);
		if ( !amountParsed || amountParsed.isNaN() || amountParsed.eq(0) ) { return true; }

		if ( !isCollateralAddressInWhitelist() ) { return true; }

		if ( isMaxCollaterals() || overMaxCollaterals() ) { return true; }

		if (
			inputCollateralAddress.toLowerCase() === token.contractAddress.toLowerCase() &&
			inputCollateralTokenId.toLowerCase() === token.tokenId.toLowerCase()
		) { return true; }

		return false;
	}
	const addCollateralERC1155Row = () => {

		if ( !collateralPopupShown ) {
			if ( token.rules.noUnwrap ) {
				setModal({
					type: _ModalTypes.info,
					title: 'Warning',
					text: [{ text: 'You can lock collateral forever with these settings', clazz: 'text-orange' }]
				});
				setCollateralPopupShown(true);
			}
		}

		const address = inputCollateralAddress;
		const tokenId = inputCollateralTokenId;
		const amount  = new BigNumber(inputCollateralAmount);

		const collateralsUpdated = [
			...collaterals.filter((item) => {
				if ( item.assetType !== _AssetType.ERC1155 ) { return true; }
				if ( !item.contractAddress ) { return false; }
				return item.contractAddress.toLowerCase() !== address.toLowerCase() ||
					item.tokenId !== tokenId
			}),
			{
				assetType: _AssetType.ERC1155,
				contractAddress: address,
				tokenId: tokenId,
				amount : amount,
			}
		];

		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
		setInputCollateralAddress('');
		setInputCollateralTokenId('');

		if ( !currentChain ) { return; }
		if ( !userAddress  ) { return; }
		getNFTById(currentChain.chainId, address, tokenId, userAddress)
			.then((data) => {
				if ( !data ){ return; }

				const collateralsUpdated = [
					...collaterals.filter((item) => {
						if ( item.assetType !== _AssetType.ERC1155 ) { return true; }
						if ( !item.contractAddress ) { return false; }
						return item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId !== tokenId
					}),
					{
						assetType: _AssetType.ERC1155,
						contractAddress: address,
						tokenId: tokenId,
						amount : amount,
						tokenImg: data.image || ''
					}
				];

				let collateralErrorsUpdated = collateralErrors;
				if ( data.amount && data.amount.lt(amount) ) {
					collateralErrorsUpdated = [
						...collateralErrorsUpdated,
						{
							contractAddress: address,
							tokenId: tokenId,
							msg: 'Not enough balance'
						}
					]
				}

				setCollaterals(collateralsUpdated);
				setCollateralErrors(collateralErrorsUpdated);

			})
			.catch((error) => {
				console.log('Cannot fetch 1155 token', error);
				const collateralsUpdated = [
					...collaterals.filter((item) => {
						if ( item.assetType !== _AssetType.ERC1155 ) { return true; }
						if ( !item.contractAddress ) { return false; }
						return item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId !== tokenId
					}),
					{
						assetType: _AssetType.ERC1155,
						contractAddress: address,
						tokenId: tokenId,
						amount : amount,
						tokenImg: ''
					}
				];

				let collateralErrorsUpdated = [
					...collateralErrors,
					{
						contractAddress: address,
						tokenId: tokenId,
						msg: 'Cannot fetch token'
					}
				]

				setCollaterals(collateralsUpdated);
				setCollateralErrors(collateralErrorsUpdated);
			})
	}
	const getAddCollateralERC1155Btn = () => {
		return (
			<button
				className="btn btn-grad"
				disabled={ isCollateralERC1155BtnDisabled() }
				onClick={() => { addCollateralERC1155Row() }}
			>Add</button>
		)
	}
	const getCollateralERC1155AddressInput = () => {
		if ( !isCollateralAddressInWhitelist() ) {
			return (
				<React.Fragment>
					<input
						className="input-control"
						type="text"
						placeholder="Paste here"
						value={ inputCollateralAddress }
						onChange={(e) => { setInputCollateralAddress( e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "") ) }}
						onKeyPress={(e) => {
							if ( e.defaultPrevented )                    { return; }
							if ( !!isCollateralERC20BtnDisabled() ) { return; }
							if ( e.key !== 'Enter' )                     { return; }

							addCollateralERC1155Row()
						}}
					/>
					<div className="input-error">Address is not in whitelist</div>
				</React.Fragment>
			)
		}
		if (
			inputCollateralAddress.toLowerCase() === token.contractAddress.toLowerCase() &&
			inputCollateralTokenId.toLowerCase() === token.tokenId.toLowerCase()
		) {
			return (
				<React.Fragment>
					<input
						className="input-control"
						type="text"
						placeholder="Paste here"
						value={ inputCollateralAddress }
							onChange={(e) => { setInputCollateralAddress( e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "") ) }}
							onKeyPress={(e) => {
								if ( e.defaultPrevented)                     { return; }
								if ( !!isCollateralERC20BtnDisabled() ) { return; }
								if ( e.key !== 'Enter' )                     { return; }

								addCollateralERC721Row()
							}}
					/>
					<div className="input-error">Cannot add token to it's collateral</div>
				</React.Fragment>
			)
		}

		return (
			<React.Fragment>
				<input
					className="input-control"
					type="text"
					placeholder="Paste here"
					value={ inputCollateralAddress }
					onChange={(e) => { setInputCollateralAddress( e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "") ) }}
					onKeyPress={(e) => {
						if ( e.defaultPrevented )                    { return; }
						if ( !!isCollateralERC20BtnDisabled() ) { return; }
						if ( e.key !== 'Enter' )                     { return; }

						addCollateralERC1155Row()
					}}
				/>
			</React.Fragment>
		)
	}
	const getCollateralERC1155Block = () => {
		if ( inputCollateralAssetType === _AssetType.ERC1155 ) {
			return (
				<div className="row">
					<div className="col col-12 col-md-5">
						<label className="input-label">NFT Address</label>
						{ getCollateralERC1155AddressInput() }
					</div>
					<div className="col col-12 col-md-3">
						<label className="input-label">Token ID</label>
						<input
							className="input-control"
							type="text"
							placeholder="99 900"
							value={ addThousandSeparator(inputCollateralTokenId) }
							onChange={(e) => { setInputCollateralTokenId( e.target.value.toLowerCase().replace(/[^0-9]/g, "") ) }}
							onKeyPress={(e) => {
								if ( e.defaultPrevented )               { return; }
								if ( !!isCollateralERC20BtnDisabled() ) { return; }
								if ( e.key !== 'Enter' )                { return; }

								addCollateralERC1155Row()
							}}
						/>
						</div>
					<div className="col col-12 col-md-2">
						<label className="input-label">Amount</label>
						<input
							className="input-control"
							type="text"
							placeholder="10"
							value={ addThousandSeparator(inputCollateralAmount) }
							onChange={(e) => {
								const value = removeThousandSeparator(e.target.value.replaceAll(' ', ''));
								if (
									value === '' ||
									isNaN(parseInt(value))
								) {
									setInputCollateralAmount('');
									return;
								}

								setInputCollateralAmount( `${parseInt(value)}` )
							}}
							onKeyPress={(e) => {
								if ( e.defaultPrevented )               { return; }
								if ( !!isCollateralERC20BtnDisabled() ) { return; }
								if ( e.key !== 'Enter' )                { return; }

								addCollateralERC20Row()
							}}
						/>
					</div>
					<div className="col col-12 col-md-2">
						<label className="input-label">&nbsp;</label>
						{ getAddCollateralERC1155Btn() }
					</div>
				</div>
			)
		}
	}
	// ----- END COLLATERAL ERC1155 -----

	const isCollateralAddressInWhitelist = () => {
		if ( whitelistContract === undefined ) { return true; }
		if ( inputCollateralAddress === '' ) { return true; }
		if ( enabledForCollateral ) {
			const foundWhitelistItem = enabledForCollateral.find((item: string) => { return item.toLowerCase() === inputCollateralAddress.toLowerCase() });
			if ( !foundWhitelistItem ) { return false; }
		}

		return true;
	}
	const isMaxCollaterals = () => {
		if ( collaterals.length === maxCollaterals ) { return true; }

		return false;
	}
	const overMaxCollaterals = () => {
		if ( collaterals.length > maxCollaterals ) { return true; }

		return false;
	}
	const getCollateralRows = () => {
		return (
			<CollateralViewer
				collaterals={
					collaterals.sort((item, prev) => {
						if ( item.assetType === prev.assetType ) {
							return item.contractAddress.localeCompare(prev.contractAddress);
						}

						return item.assetType - prev.assetType
					})
				}
				collateralErrors={ collateralErrors }
				removeRow={(item: CollateralItem) => {
					let collateralsUpdated = collaterals

					collateralsUpdated = [
						...collateralsUpdated.filter((iitem) => { return iitem.contractAddress.toLowerCase() !== item.contractAddress.toLowerCase() || ( iitem.tokenId && item.tokenId && iitem.tokenId.toLowerCase() !== item.tokenId.toLowerCase() ) }),
					]

					setCollaterals(collateralsUpdated);
					setCollateralErrors(collateralErrors.filter((iitem) => { return item.contractAddress.toLowerCase() !== iitem.contractAddress.toLowerCase() || item.tokenId !== iitem.tokenId }));
				}}
				width="wide"
				color="light"
			/>
		)
	}
	const getMaxCollateralAlert = () => {
		if ( !collaterals.length ) { return null; }
		if ( overMaxCollaterals() ) { return ( <div className="alert alert-error mb-3">You have changed max collateral slots. Added amount exceeds allowed amount</div> ) }
		if ( isMaxCollaterals() ) { return ( <div className="alert alert-warning mb-3">You have reached the maximum number of collateral slots</div> ) }

		return null;
	}
	const getCollateralBlock = () => {

		if ( token.rules.noAddCollateral ) { return null; }

		return (
			<>
				{ getMaxCollateralAlert() }
				<div className="alert mb-4">You can add assets to collateral of your wrapped NFT. Use list of approved tokens.</div>
				<div className="c-wrap__form">
					<div className="row row-sm mb-5">
						<div className="col-12 col-sm-auto mr-3 py-2">
							<label className="input-label mb-0">Token Type</label>
						</div>
						<div className="col-auto py-2">
							<label className="checkbox">
								<input
									type="radio"
									name="token-type"
									value={ _AssetType.native }
									checked={ inputCollateralAssetType === _AssetType.native }
									onChange={() => {
										setInputCollateralAssetType(_AssetType.native);
										setInputCollateralAddress('');
										setInputCollateralTokenId('');
										setInputCollateralAmount('');
									}}
								/>
								<span className="check"> </span>
								<span className="check-text">Native</span>
							</label>
						</div>
						<div className="col-auto py-2">
							<label className="checkbox">
								<input
									type="radio"
									name="token-type"
									value={ _AssetType.ERC20 }
									checked={ inputCollateralAssetType === _AssetType.ERC20 }
									onChange={() => {
										setInputCollateralAssetType(_AssetType.ERC20);
										setInputCollateralAddress('');
										setInputCollateralTokenId('');
										setInputCollateralAmount('');
									}}
								/>
								<span className="check"> </span>
								<span className="check-text">{ currentChain?.EIPPrefix || 'ERC' }-20</span>
							</label>
						</div>
						<div className="col-auto py-2">
							<label className="checkbox">
								<input
									type="radio"
									name="token-type"
									value={ _AssetType.ERC721 }
									checked={ inputCollateralAssetType === _AssetType.ERC721 }
									onChange={() => {
										setInputCollateralAssetType(_AssetType.ERC721);
										setInputCollateralAddress('');
										setInputCollateralTokenId('');
										setInputCollateralAmount('');
									}}
								/>
								<span className="check"> </span>
								<span className="check-text">{ currentChain?.EIPPrefix || 'ERC' }-721</span>
							</label>
						</div>
						<div className="col-auto py-2">
							<label className="checkbox">
								<input
									type="radio"
									name="token-type"
									value={ _AssetType.ERC1155 }
									checked={ inputCollateralAssetType === _AssetType.ERC1155 }
									onChange={() => {
										setInputCollateralAssetType(_AssetType.ERC1155);
										setInputCollateralAddress('');
										setInputCollateralTokenId('');
										setInputCollateralAmount('');
									}}
								/>
								<span className="check"> </span>
								<span className="check-text">{ currentChain?.EIPPrefix || 'ERC' }-1155</span>
							</label>
						</div>
					</div>
					{ getCollateralNativeBlock() }
					{ getCollateralERC20Block() }
					{ getCollateralERC721Block() }
					{ getCollateralERC1155Block() }
				</div>

				<div className="text-right">
					<b>{ collaterals.length }</b> / <span className="text-muted">{ maxCollaterals } entries</span>
				</div>

				{ getCollateralRows() }
			</>
		)
	}
	// ----- END COLLATERAL -----


	const getV0Label = () => {
		if ( !currentChain ) { return null; }
		if ( !token || token.assetType !== _AssetType.wNFTv0 ) { return null; }

		const tokenUrl = `${v0AppLink}/#/token?chain=${currentChain?.chainId}&contractAddress=${ token.contractAddress }&tokenId=${ token.tokenId }`

		return (
			<React.Fragment>
				<p className="text-orange">This NFT has been wrapped by the previous version of dApp. Some functions may not work properly with current version of dApp.<br /><a target="_blank" rel="noopener noreferrer" href={ tokenUrl } >Go to v0 app.</a></p>
			</React.Fragment>
		)
	}

	const addressIsStorage = (address: string): boolean => {
		return !!storageContracts.find((item => { return item.toLowerCase() === address.toLowerCase() }))
	}
	const createAdvancedLoaderAddValue = (_currentChain: ChainType, _web3: Web3, _userAddress: string) => {
		const loaderStages: Array<AdvancedLoaderStageType> = []

		const qtyERC20Collaterals = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC20
		});
		if ( qtyERC20Collaterals.length ) {
			loaderStages.push({
				id: 'approve20collateral',
				sortOrder: 10,
				text: `Approving ${_currentChain.EIPPrefix}-20 collateral tokens`,
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyERC20Collaterals.length,
			});
		}
		const qtyERC721Collaterals = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC721
		});
		if ( qtyERC721Collaterals.length ) {
			loaderStages.push({
				id: 'approve721collateral',
				sortOrder: 11,
				text: `Approving ${_currentChain.EIPPrefix}-721 collateral tokens`,
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyERC721Collaterals.length,
			});
		}
		const qtyERC1155Collaterals = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC1155
		});
		if ( qtyERC1155Collaterals.length ) {
			loaderStages.push({
				id: 'approve1155collateral',
				sortOrder: 12,
				text: `Approving ${_currentChain.EIPPrefix}-1155 collateral tokens`,
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyERC1155Collaterals.length,
			});
		}

		const qtyWNFTCollateralFees = collaterals.filter((item) => {
			return addressIsStorage( item.contractAddress );
		});
		if ( qtyWNFTCollateralFees.length ) {
			loaderStages.push({
				id: 'approvewnftcollateralfees',
				sortOrder: 15,
				text: 'Approving WNFT collateral fees',
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyWNFTCollateralFees.length,
			});
		}

		loaderStages.push({
			id: 'addvalue',
			sortOrder: 20,
			text: 'Adding value',
			status: _AdvancedLoadingStatus.queued
		});

		const advLoader = {
			title: 'Waiting to add value',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);
	}
	const approve20Collateral = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		const erc20CollateralsToCheck = collaterals.filter((item) => { return item.assetType === _AssetType.ERC20 });
		if ( !erc20CollateralsToCheck.length ) { return; }

		for (let idx = 0; idx < erc20CollateralsToCheck.length; idx++) {
			const item = erc20CollateralsToCheck[idx];

			let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() });
			if ( !foundToken ) {
				foundToken = getNullERC20(item.contractAddress);
			}

			updateStepAdvancedLoader({
				id: 'approve20collateral',
				status: _AdvancedLoadingStatus.loading,
				text: `Approving ${ foundToken.symbol }`,
				current: idx + 1,
			});

			if ( !item.amount ) { continue; }

			if ( item.contractAddress.toLowerCase() === techToken.toLowerCase() ) { continue; }

			const balance = await getERC20BalanceFromChain(_currentChain.chainId, item.contractAddress, _userAddress, wrapperContract);
			if ( balance.balance.lt(item.amount) ) {
				setModal({
					type: _ModalTypes.error,
					title: `Not enough ${foundToken.symbol}`,
					details: [
						`Token ${foundToken.symbol}: ${item.contractAddress}`,
						`User address: ${_userAddress}`,
						`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
					]
				});
				throw new Error();
			}
			if ( !balance.allowance || balance.allowance.amount.lt(item.amount) ) {
				try {
					if ( isMultisig ) {
						await makeERC20AllowanceMultisig(_web3, item.contractAddress, _userAddress, item.amount, wrapperContract);
					} else {
						await makeERC20Allowance(_web3, item.contractAddress, _userAddress, item.amount, wrapperContract);
					}
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${foundToken.symbol}`,
						details: [
							`Token ${foundToken.symbol}: ${item.contractAddress}`,
							`User address: ${_userAddress}`,
							`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
							'',
							e.message || e,
						]
					});
					throw new Error();
				}
			}

		}

		updateStepAdvancedLoader({
			id: 'approve20collateral',
			text: `Approving ${_currentChain.EIPPrefix}-20 collateral tokens`,
			status: _AdvancedLoadingStatus.complete
		});

	}
	const approve721Collateral = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		const erc721CollateralsToCheck = collaterals.filter((item) => { return item.assetType === _AssetType.ERC721 });
		if ( !erc721CollateralsToCheck.length ) { return; }

		for (let idx = 0; idx < erc721CollateralsToCheck.length; idx++) {
			const item = erc721CollateralsToCheck[idx];

			if ( !item.tokenId ) { continue; }

			updateStepAdvancedLoader({
				id: 'approve721collateral',
				text: `Approving ${_currentChain.EIPPrefix}-721: ${compactString(item.contractAddress)}: ${item.tokenId}`,
				status: _AdvancedLoadingStatus.loading
			});

			const isApproved = await checkApprovalERC721(_currentChain.chainId, item.contractAddress, item.tokenId, _userAddress, wrapperContract);
			if ( !isApproved ) {
				try {
					if ( isMultisig ) {
						await setApprovalERC721Multisig(_web3, item.contractAddress, item.tokenId, _userAddress, wrapperContract)
					} else {
						await setApprovalERC721(_web3, item.contractAddress, item.tokenId, _userAddress, wrapperContract)
					}
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${_currentChain.EIPPrefix}-721`,
						details: [
							`Contract address: ${item.contractAddress}`,
							`Token id: ${item.tokenId}`,
							`User address: ${_userAddress}`,
							'',
							e.message || e,
						]
					});
					throw new Error();
				}
			}

		}

		updateStepAdvancedLoader({
			id: 'approve721collateral',
			text: `Approving ${_currentChain.EIPPrefix}-721 collateral tokens`,
			status: _AdvancedLoadingStatus.complete
		});
	}
	const approve1155Collateral = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		const erc1155CollateralsToCheck = collaterals.filter((item) => { return item.assetType === _AssetType.ERC1155 });
		if ( !erc1155CollateralsToCheck.length ) { return; }

		for (let idx = 0; idx < erc1155CollateralsToCheck.length; idx++) {
			const item = erc1155CollateralsToCheck[idx];

			updateStepAdvancedLoader({
				id: 'approve1155collateral',
				text: `Approving ${_currentChain.EIPPrefix}-1155: ${compactString(item.contractAddress)}`,
				status: _AdvancedLoadingStatus.loading
			});

			const isApproved = await checkApprovalERC1155(_currentChain.chainId, item.contractAddress, _userAddress, wrapperContract);
			if ( !isApproved ) {
				try {
					if ( isMultisig ) {
						await setApprovalERC1155Multisig(_web3, item.contractAddress, _userAddress, wrapperContract)
					} else {
						await setApprovalERC1155(_web3, item.contractAddress, _userAddress, wrapperContract)
					}
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${_currentChain.EIPPrefix}-1155`,
						details: [
							`Contract address: ${item.contractAddress}`,
							`User address: ${_userAddress}`,
							'',
							e.message || e,
						]
					});
					throw new Error();
				}
			}

		}

		updateStepAdvancedLoader({
			id: 'approve1155collateral',
			text: `Approving ${_currentChain.EIPPrefix}-1155 collateral tokens`,
			status: _AdvancedLoadingStatus.complete
		});
	}
	const approveWNFTFees = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		const wnftCollateralsToCheck = collaterals.filter((item) => { return addressIsStorage(item.contractAddress) });
		if ( !wnftCollateralsToCheck.length ) { return; }

		for (let idx = 0; idx < wnftCollateralsToCheck.length; idx++) {
			const item = wnftCollateralsToCheck[idx];

			if ( !item.tokenId ) { continue; }

			updateStepAdvancedLoader({
				id: 'approvewnftcollateralfees',
				text: `Approving fee of ${compactString(item.contractAddress)}: ${item.tokenId}`,
				status: _AdvancedLoadingStatus.loading
			});

			const wnft = await getWNFTById(_currentChain.chainId, item.contractAddress, item.tokenId);
			if ( !wnft || !wnft.fees.length ) { continue; }

			const fee = wnft.fees[0];

			if ( fee.token.toLowerCase() === techToken.toLowerCase() ) { continue; }

			let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === fee.token.toLowerCase() });
			if ( !foundToken ) { foundToken = getNullERC20(fee.token) }

			updateStepAdvancedLoader({
				id: 'approvewnftcollateralfees',
				text: `Approving (${foundToken.symbol}) of ${compactString(item.contractAddress)}: ${item.tokenId}`,
				status: _AdvancedLoadingStatus.loading
			});

			// erc20 collateral of same token
			// without summing this tx will overwrite approval amount
			const erc20Collateral = collaterals.find((iitem) => { return iitem.contractAddress.toLowerCase() === fee.token.toLowerCase() });
			const amountToCheck = fee.value.plus(erc20Collateral?.amount || new BigNumber(0));
			const balance = await getERC20BalanceFromChain(_currentChain.chainId, fee.token, _userAddress, wrapperContract);
			if ( balance.balance.lt(amountToCheck) ) {
				setModal({
					type: _ModalTypes.error,
					title: `Not enough ${foundToken.symbol}`,
					details: [
						`Token ${foundToken.symbol}: ${fee.token}`,
						`User address: ${_userAddress}`,
						`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
					]
				});
				throw new Error();
			}
			if ( !balance.allowance || balance.allowance.amount.lt(amountToCheck) ) {
				try {
					if ( isMultisig ) {
						await makeERC20AllowanceMultisig(_web3, fee.token, _userAddress, amountToCheck, wrapperContract);
					} else {
						await makeERC20Allowance(_web3, fee.token, _userAddress, amountToCheck, wrapperContract);
					}
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${foundToken.symbol}`,
						details: [
							`Token ${foundToken.symbol}: ${fee.token}`,
							`User address: ${_userAddress}`,
							`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
							'',
							e.message || e,
						]
					});
					throw new Error();
				}
			}

		}

		updateStepAdvancedLoader({
			id: 'approvewnftcollateralfees',
			text: `Approving WNFT collateral fees`,
			status: _AdvancedLoadingStatus.complete
		});
	}
	const addValueSubmit = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {
		createAdvancedLoaderAddValue(_currentChain, _web3, _userAddress);

		try { await approve20Collateral(_currentChain, _web3, _userAddress, isMultisig);   } catch(ignored) { return; }
		try { await approve721Collateral(_currentChain, _web3, _userAddress, isMultisig);  } catch(ignored) { return; }
		try { await approve1155Collateral(_currentChain, _web3, _userAddress, isMultisig); } catch(ignored) { return; }
		try { await approveWNFTFees(_currentChain, _web3, _userAddress, isMultisig);       } catch(ignored) { return; }

		updateStepAdvancedLoader({
			id: 'addvalue',
			status: _AdvancedLoadingStatus.loading
		});

		let txResp: any;
		try {
			if ( isMultisig ) {
				txResp = await addValueToWNFTMultisig(_web3, token, collaterals, _userAddress);
			} else {
				txResp = await addValueToWNFT(_web3, token, collaterals, _userAddress);
			}
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot add value`,
				details: [
					`Token ${token.contractAddress}: ${token.tokenId}`,
					`User address: ${_userAddress}`,
					`Collaterals: ${JSON.stringify(collaterals)}`,
					'',
					e.message || e,
				]
			});
			return;
		}

		updateStepAdvancedLoader({
			id: 'addvalue',
			status: _AdvancedLoadingStatus.complete
		});
		unsetModal();

		if ( isMultisig ) {
			setModal({
				type: _ModalTypes.success,
				title: `Transactions have been created`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						window.location.href = '/dashboard';
					}
				}],
			});
		} else {
			setModal({
				type: _ModalTypes.success,
				title: `Value successfully added`,
				text: currentChain?.hasOracle ? [
					{ text: 'After oracle synchronization, your changes will appear on dashboard soon. Please refresh the dashboard page' },
				] : [],
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						window.location.href = '/dashboard';
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		}
	}
	const getAddValueBtn = () => {
		return (
			<div className="row mt-5">
			<div className="col col-12 col-md-6 offset-md-3">
				<button
					className="btn btn-lg w-100"
					disabled={
						!collaterals.length ||
						!!collateralErrors.length
					}
					onClick={async () => {
						if ( !currentChain ) { return; }
						let _web3 = web3;
						let _userAddress = userAddress;
						let _currentChain = currentChain;

						if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
							setLoading('Waiting for wallet');

							try {
								const web3Params = await getWeb3Force(_currentChain.chainId);

								_web3 = web3Params.web3;
								_userAddress = web3Params.userAddress;
								_currentChain = web3Params.chain;
								unsetModal();
							} catch(e: any) {
								setModal({
									type: _ModalTypes.error,
									title: 'Error with wallet',
									details: [
										e.message || e
									]
								});
							}
						}

						if ( !_web3 || !_userAddress || !_currentChain ) { return; }
						const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
						addValueSubmit(_currentChain, _web3, _userAddress, isMultisig);
					}}
				>Confirm</button>
			</div>
			</div>
		)
	}


	return (
		<div className="modal modal-lg">
			<div className="modal__inner">
				<div className="modal__bg"></div>
				<div className="container">
					<div className="modal__content">
						<div
							className="modal__close"
							onClick={() => { closePopup() }}
						>
							<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
								<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
							</svg>
						</div>


						<div className="c-add">
							<div className="c-add__text">
								<div className="h2">Add Collateral</div>
								<div className="d-flex">
									<img className="mr-2" src={ icon_attention } alt="" />
									<span className="text-muted">мах { maxCollaterals } entries</span>
								</div>
								{ getV0Label() }
							</div>
							{ getCollateralBlock() }
						</div>

						{ getAddValueBtn() }

					</div>
				</div>
			</div>
		</div>
	)
}
